-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a MIPS_Processor  
-- implementation.

-- 01/29/2019 by H3::Design created.
-------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY work;
USE work.MIPS_types.ALL;

ENTITY MIPS_Processor IS
  GENERIC (N : INTEGER := DATA_WIDTH);
  PORT (
    iCLK      : IN STD_LOGIC;
    iRST      : IN STD_LOGIC;
    iInstLd   : IN STD_LOGIC;
    iInstAddr : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    iInstExt  : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    oALUOut   : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

END MIPS_Processor;
ARCHITECTURE structure OF MIPS_Processor IS

  -- Required data memory signals
  SIGNAL s_DMemWr   : STD_LOGIC;                        -- TODO: use this signal as the final active high data memory write enable signal
  SIGNAL s_DMemAddr : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the final data memory address input
  SIGNAL s_DMemData : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the final data memory data input
  SIGNAL s_DMemOut  : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the data memory output

  -- Required register file signals 
  SIGNAL s_RegWr     : STD_LOGIC;                        -- TODO: use this signal as the final active high write enable input to the register file
  SIGNAL s_RegWrAddr : STD_LOGIC_VECTOR(4 DOWNTO 0);     -- TODO: use this signal as the final destination register address input
  SIGNAL s_RegWrData : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the final data memory data input

  -- Required instruction memory signals
  SIGNAL s_IMemAddr     : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  SIGNAL s_NextInstAddr : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as your intended final instruction memory address input.
  SIGNAL s_Inst         : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the instruction signal 

  -- Required halt signal -- for simulation
  SIGNAL s_Halt : STD_LOGIC; -- TODO: this signal indicates to the simulation that intended program execution has completed. (Opcode: 01 0100)

  -- Required overflow signal -- for overflow exception detection
  SIGNAL s_Ovfl : STD_LOGIC; -- TODO: this signal indicates an overflow exception would have been initiated

  -- TODO: You may add any additional signals or components your implementation 
  --       requires below this comment

  COMPONENT mem IS
    GENERIC (
      ADDR_WIDTH : INTEGER;
      DATA_WIDTH : INTEGER);
    PORT (
      clk  : IN STD_LOGIC;
      addr : IN STD_LOGIC_VECTOR((ADDR_WIDTH - 1) DOWNTO 0);
      data : IN STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0);
      we   : IN STD_LOGIC := '1';
      q    : OUT STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0));
  END COMPONENT;

  COMPONENT MIPS_regfile IS
    PORT (
      i_data  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_write : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_en    : IN STD_LOGIC;
      clk     : IN STD_LOGIC;
      reset   : IN STD_LOGIC;
      i_readA : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_readB : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_A     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_B     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT IF_ID_Reg IS
    PORT (
      i_CLK         : IN STD_LOGIC;
      i_RST         : IN STD_LOGIC;
      i_PC4         : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_instruction : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_instruction : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT ID_EX_Reg IS
    PORT (
      i_CLK          : IN STD_LOGIC;
      i_RST          : IN STD_LOGIC;
      i_PC4          : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_readA        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_readB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_signExtImmed : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      --i_JA           : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
      i_inst20_16    : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_inst15_11    : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      --i_control      : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
      i_RegDst       : IN STD_LOGIC;
      i_RegWrite     : IN STD_LOGIC;
      i_memToReg     : IN STD_LOGIC;
      i_MemWrite     : IN STD_LOGIC;
      i_ALUSrc       : IN STD_LOGIC;
      i_ALUOp        : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      i_jal          : IN STD_LOGIC;
      i_halt         : IN STD_LOGIC;
      --i_memRd : in std_logic;
      o_PC4          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_readA        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_readB        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_signExtImmed : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      --o_JA           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
      o_inst20_16    : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_inst15_11    : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      --o_control      : OUT STD_LOGIC_VECTOR(14 DOWNTO 0)
      o_RegDst   : OUT STD_LOGIC;
      o_RegWrite : OUT STD_LOGIC;
      o_memToReg : OUT STD_LOGIC;
      o_MemWrite : OUT STD_LOGIC;
      o_ALUSrc   : OUT STD_LOGIC;
      o_ALUOp    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      o_jal      : OUT STD_LOGIC;
      o_halt     : OUT STD_LOGIC
      --o_memRd : out std_logic
    );
  END COMPONENT;

  COMPONENT EX_MEM IS
    PORT (
      i_CLK      : IN STD_LOGIC;                      -- Clock input
      i_RST      : IN STD_LOGIC;                      -- Reset input
      i_WE       : IN STD_LOGIC;                      -- Write enable input
      i_ALU      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- ALU value input
      o_ALU      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); -- ALU value output
      i_B        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  --Mem address value
      o_B        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Mem address value
      i_WrAddr   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);   --write address to register file
      o_WrAddr   : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_MemWr    : IN STD_LOGIC;  --write to memory
      o_MemWr    : OUT STD_LOGIC; --write to memory signal
      --i_MemRd    : IN STD_LOGIC;  --read memory
      --o_MemRd    : OUT STD_LOGIC; --read memory signal
      i_MemtoReg : IN STD_LOGIC;  --memory vs alu signal
      o_MemtoReg : OUT STD_LOGIC;
      i_Halt     : IN STD_LOGIC; --Halt signal
      o_Halt     : OUT STD_LOGIC;
      i_RegWr : in std_logic; --write enable for reg file
        o_RegWr : out std_logic;
        i_jal : in std_logic;
        o_jal : out std_logic;
        i_PC4 : in std_logic_vector(31 downto 0); --pc+4
        o_PC4 : out std_logic_vector(31 downto 0)
    );
  END COMPONENT;

  COMPONENT MEM_WB IS
    PORT (
      i_CLK      : IN STD_LOGIC;                      -- Clock input
      i_RST      : IN STD_LOGIC;                      -- Reset input
      i_WE       : IN STD_LOGIC;                      -- Write enable input
      i_ALU      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- ALU value input
      o_ALU      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); -- ALU value output
      i_Mem      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  --Mem value
      o_Mem      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Mem value
      i_WrAddr   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);   --write address to register file
      o_WrAddr   : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_MemtoReg : IN STD_LOGIC; --memory vs alu signal
      o_MemtoReg : OUT STD_LOGIC;
      i_Halt     : IN STD_LOGIC; --halt signal
      o_Halt     : OUT STD_LOGIC;
      i_RegWr : in std_logic; --write enable for reg file
        o_RegWr : out std_logic;
        i_jal : in std_logic;
        o_jal : out std_logic;
        i_PC4 : in std_logic_vector(31 downto 0); --pc+4
        o_PC4 : out std_logic_vector(31 downto 0)
    );
  END COMPONENT;

  COMPONENT ALU IS
    PORT (
      i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_ALUOP    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      i_shamt    : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_resultF  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_CarryOut : OUT STD_LOGIC;
      o_Overflow : OUT STD_LOGIC;
      o_zero     : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT Full_Adder_N IS
    PORT (
      i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_C        : IN STD_LOGIC;
      o_S        : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_C        : OUT STD_LOGIC;
      o_Overflow : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT Fetch_Pipeline IS
    PORT (
      i_PC4         : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --Program counter
      i_branch_addr : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --potential branch address
      i_jump_addr   : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --potential jump address
      i_jr          : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --potential jump register address
      i_jr_select   : IN STD_LOGIC;                     --selector for jump register
      i_branch      : IN STD_LOGIC;                     --selects if a branch instruction is active
      i_bne         : in std_logic;
      --i_zero        : IN STD_LOGIC;                     --if branch, determines if you should branch
      i_A : in std_logic_vector(31 downto 0);
      i_B : in std_logic_vector(31 downto 0);
      i_jump        : IN STD_LOGIC;                     --selector for j or jal
      o_PC          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      --o_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      o_jump_branch : out std_logic --signals if PC must change
      );
  END COMPONENT;

  COMPONENT MIPS_extender IS
    PORT (
      i_sign : IN STD_LOGIC;                     --determines if the value must be sign or zero extended
      i_in   : IN STD_LOGIC_VECTOR(15 DOWNTO 0); --value to be extended
      o_O    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT mux2t1_N IS
    GENERIC (N : INTEGER);
    PORT (
      i_S  : IN STD_LOGIC;
      i_D0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT mux2t1_5bit IS
    GENERIC (N : INTEGER);
    PORT (
      i_S  : IN STD_LOGIC;
      i_D0 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(4 DOWNTO 0));
  END COMPONENT;

  --PC register
  COMPONENT dffg_N IS
    PORT (
      i_CLK : IN STD_LOGIC;                     -- Clock input
      i_RST : IN STD_LOGIC;                     -- Reset input
      i_WE  : IN STD_LOGIC;                     -- Write enable input
      i_D   : IN STD_LOGIC_VECTOR(31 DOWNTO 0); -- Data value input
      o_Q   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT Control_Unit IS
    PORT (
      i_opCode    : IN STD_LOGIC_VECTOR(5 DOWNTO 0); --MIPS instruction opcode (6 bits wide)
      i_funct     : IN STD_LOGIC_VECTOR(5 DOWNTO 0); --MIPS instruction function code (6 bits wide) used for R-Type instructions
      o_RegDst    : OUT STD_LOGIC;
      o_RegWrite  : OUT STD_LOGIC;
      o_memToReg  : OUT STD_LOGIC;
      o_memWrite  : OUT STD_LOGIC;
      o_ALUSrc    : OUT STD_LOGIC;
      o_ALUOp     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      o_signed    : OUT STD_LOGIC;
      o_addSub    : OUT STD_LOGIC;
      o_shiftType : OUT STD_LOGIC;
      o_shiftDir  : OUT STD_LOGIC;
      o_bne       : OUT STD_LOGIC;
      o_beq       : OUT STD_LOGIC;
      o_j         : OUT STD_LOGIC;
      o_jr        : OUT STD_LOGIC;
      o_jal       : OUT STD_LOGIC;
      o_branch    : OUT STD_LOGIC;
      o_jump      : OUT STD_LOGIC;
      o_lui       : OUT STD_LOGIC;
      o_halt      : OUT STD_LOGIC);
  END COMPONENT;

  --signals 
  SIGNAL s_RegDst, s_memToReg, s_memWrite, s_ALUSrc, s_j, s_jr, s_jal, s_branch, s_jump : STD_LOGIC;
  SIGNAL s_ALUOp, s_EXALUOp                                                                        : STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL s_signed, s_lui, s_addSub, s_shiftType, s_shiftDir, s_bne, s_beq               : STD_LOGIC;
  SIGNAL s_RegA, s_RegB                                                                 : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL s_rtrd, s_EXrtrd, s_MEMrtrd, s_WBrtrd                                                    : STD_LOGIC_VECTOR(4 DOWNTO 0);
  SIGNAL s_immediate                                                                    : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL s_PC, s_PCR                                                                    : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL s_PC4, s_nextPC                                                                          : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL s_zero, s_CarryOut                                                             : STD_LOGIC;
  SIGNAL s_ALUB, s_aluORmem                                                             : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL s_IF_PC4, s_EX_PC4, s_MEM_PC4, s_WB_PC4                                                                       : STD_LOGIC_VECTOR(31 DOWNTO 0); --instruction fetch PC+4 signal
  SIGNAL s_ID_Inst                                                                      : STD_LOGIC_VECTOR(31 DOWNTO 0); --instruction from IF/ID reg
  SIGNAL s_ID_PC4                                                                       : STD_LOGIC_VECTOR(31 DOWNTO 0); --instruction for PC+4 after IF/ID reg
  SIGNAL s_EXA, s_EXB, s_EXImmediate                                                    : STD_LOGIC_VECTOR(31 DOWNTO 0); --signals for ALU after ID/EX reg
  SIGNAL s_EXrt, s_EXrd                                                                 : STD_LOGIC_VECTOR(4 DOWNTO 0);
  SIGNAL s_ALUOut, s_MEMALU, s_WBALU                                                    : STD_LOGIC_VECTOR(31 DOWNTO 0); --output of the ALU
  SIGNAL s_MEMData                                                                      : STD_LOGIC_VECTOR(31 DOWNTO 0); --value to be written to memory
  SIGNAL s_MEMOut, s_WBMEMOut                                                           : STD_LOGIC_VECTOR(31 DOWNTO 0); --memory output value
  signal s_EXRegDst, s_EXRegWr, s_EXmemToReg, s_EXMemWr, s_EXMemRd, s_EXALUSrc, s_EXjal, s_EXhalt : std_logic; --ex signal stages
  signal s_MEMRegWr, s_MEMMemWr, s_MEMMemRd, s_MEMmemtoReg, s_MEMhalt : std_logic;
  signal s_WBmemToReg, s_WBRegWr : std_logic;
  signal s_IDhalt, s_IDMemWr : std_logic;
  signal s_jump_branch : std_logic; --show if jump or branch
  signal s_regEnable, s_IDRegWr : std_logic;
  signal s_MEMjal, s_WBjal : std_logic;
  
  

BEGIN

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  WITH iInstLd SELECT
    s_IMemAddr <= s_NextInstAddr WHEN '0',
    iInstAddr WHEN OTHERS;
  IMem : mem
  GENERIC MAP(
    ADDR_WIDTH => ADDR_WIDTH,
    DATA_WIDTH => N)
  PORT MAP(
    clk  => iCLK,
    addr => s_IMemAddr(11 DOWNTO 2),
    data => iInstExt,
    we   => iInstLd,
    q    => s_Inst);

  DMem : mem
  GENERIC MAP(
    ADDR_WIDTH => ADDR_WIDTH,
    DATA_WIDTH => N)
  PORT MAP(
    clk  => iCLK,
    addr => s_DMemAddr(11 DOWNTO 2),
    data => s_DMemData,
    we   => s_DMemWr,
    q    => s_DMemOut);

  -- TODO: Ensure that s_Halt is connected to an output control signal produced from decoding the Halt instruction (Opcode: 01 0100)
  -- TODO: Ensure that s_Ovfl is connected to the overflow output of your ALU

  -- TODO: Implement the rest of your processor below this comment! 

  s_DMemAddr <= s_MEMALU;

  s_RegWr <= s_WBRegWr;

  s_RegWrAddr <= s_WBrtrd;

  RegFile : MIPS_regfile
  PORT MAP(
    i_data  => s_RegWrData,
    i_write => s_RegWrAddr,
    i_en    => s_RegWr,
    clk     => iCLK,
    reset   => iRST,
    i_readA => s_ID_Inst(25 DOWNTO 21),
    i_readB => s_ID_Inst(20 DOWNTO 16),
    o_A     => s_RegA,
    o_B     => s_RegB
  );

  rtrdMUX : mux2t1_5bit
  GENERIC MAP(N => 5)
  PORT MAP(
    i_S  => s_EXRegDst,
    i_D0 => s_EXrt,
    i_D1 => s_EXrd,
    o_O  => s_rtrd
  );

  writeMUX : mux2t1_5bit
  GENERIC MAP(n => 5)
  PORT MAP(
    i_S  => s_EXjal,
    i_D0 => s_rtrd,
    i_D1 => "11111",
    o_O  => s_EXrtrd
  );

  Control : Control_Unit
  PORT MAP(
    i_opCode    => s_ID_Inst(31 DOWNTO 26),
    i_funct     => s_ID_Inst(5 DOWNTO 0),
    o_RegDst    => s_RegDst,
    o_RegWrite  => s_IDRegWr,
    o_memToReg  => s_memToReg,
    o_memWrite  => s_IDMemWr,
    o_ALUSrc    => s_ALUSrc,
    o_ALUOp     => s_ALUOp,
    o_signed    => s_signed,
    o_addSub    => s_addSub,
    o_shiftType => s_shiftType,
    o_shiftDir  => s_shiftDir,
    o_bne       => s_bne,
    o_beq       => s_beq,
    o_j         => s_j,
    o_jr        => s_jr,
    o_jal       => s_jal,
    o_branch    => s_branch,
    o_jump      => s_jump,
    o_lui       => s_lui,
    o_halt      => s_IDhalt
  );

  PC : dffg_N
  PORT MAP(
    i_CLK => iCLK,
    i_RST => '0',
    i_WE  => '1',
    i_D   => s_PCR,
    o_Q   => s_NextInstAddr
  );

  PCRESET : mux2t1_N
  GENERIC MAP(N => 32)
  PORT MAP(
    i_S  => iRST,
    i_D0 => s_nextPC,
    i_D1 => x"00400000",
    o_O  => s_PCR
  );


  NEXTPC : mux2t1_N
  generic map(N => 32)
  port map(
    i_S => s_jump_branch,
    i_D0 => s_IF_PC4,
    i_D1 => s_PC,
    o_O => s_nextPC
  );

  Fetch : Fetch_Pipeline
  PORT MAP(
    i_PC4          => s_ID_PC4,
    i_branch_addr => s_immediate,
    i_jump_addr   => s_ID_Inst,
    i_jr          => s_RegA,
    i_jr_select   => s_jr,
    i_branch      => s_branch,
    i_bne => s_bne,
    --i_zero        => s_zero,
    i_A => s_RegA,
    i_B => s_RegB,
    i_jump        => s_jump,
    o_PC          => s_PC,
    o_jump_branch => s_jump_branch
    --o_PC4         => s_PC4
  );

  SignExtend : MIPS_extender
  PORT MAP(
    i_sign => s_signed,
    i_in   => s_ID_Inst(15 DOWNTO 0),
    o_O    => s_immediate
  );

  immediateMUX : mux2t1_N
  GENERIC MAP(N => 32)
  PORT MAP(
    i_S  => s_EXALUSrc,
    i_D0 => s_EXB,
    i_D1 => s_EXImmediate,
    o_O  => s_ALUB
  );

  mainALU : ALU
  PORT MAP(
    i_A        => s_EXA,
    i_B        => s_ALUB,
    i_ALUOP    => s_EXALUOp,
    i_shamt    => s_EXImmediate(10 DOWNTO 6),
    o_resultF  => s_ALUOut,
    o_CarryOut => s_CarryOut,
    o_Overflow => s_Ovfl,
    o_zero     => s_zero
  );

  oALUOut <= s_ALUOut;

  memtoreg : mux2t1_N
  GENERIC MAP(N => 32)
  PORT MAP(
    i_S  => s_WBmemToReg,
    i_D0 => s_WBALU,
    i_D1 => s_WBMemOut,
    o_O  => s_aluORmem
  );

  raMUX : mux2t1_N
  GENERIC MAP(N => 32)
  PORT MAP(
    i_S  => s_WBjal,
    i_D0 => s_aluORmem,
    i_D1 => s_WB_PC4,
    o_O  => s_RegWrData
  );


  PC4Add : Full_Adder_N
  PORT MAP(
    i_A => s_NextInstAddr,
    i_B => x"00000004",
    i_C => '0',
    o_S => s_IF_PC4
  );

  --State Registers

  IF_ID : IF_ID_Reg
  PORT MAP(
    i_CLK         => iCLK,
    i_RST         => iRST,
    i_PC4         => s_IF_PC4,
    i_instruction => s_Inst,
    o_PC4         => s_ID_PC4,
    o_instruction => s_ID_Inst
  );

  ID_EX : ID_EX_Reg
  PORT MAP(
    i_CLK => iCLK,
    i_RST => iRST,
    i_PC4 => s_ID_PC4,
    i_readA        => s_RegA,
    i_readB        => s_RegB,
    i_signExtImmed => s_immediate,
    --i_JA : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
    i_inst20_16 => s_ID_Inst(20 DOWNTO 16),
    i_inst15_11 => s_ID_Inst(15 DOWNTO 11),
    --i_control             : in std_logic_vector(14 downto 0);
    i_RegDst    => s_RegDst,
    i_RegWrite => s_IDRegWr,
    i_memToReg => s_memToReg,
    i_MemWrite => s_IDMemWr, 
    i_ALUSrc   => s_ALUSrc,
    i_ALUOp    => s_ALUOp,
    i_jal     => s_jal,
    i_halt    => s_IDhalt,
    --i_memRd  => 
    o_PC4     => s_EX_PC4,
    o_readA        => s_EXA,
    o_readB        => s_EXB,
    o_signExtImmed => s_EXImmediate,
    --o_JA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
    o_inst20_16 => s_EXrt,
    o_inst15_11 => s_EXrd,
    --o_control             : out std_logic_vector(14 downto 0)
    o_RegDst   => s_EXRegDst,
    o_RegWrite => s_EXRegWr,
    o_memToReg => s_EXmemToReg,
    o_MemWrite => s_EXMemWr,
    o_ALUSrc   => s_EXALUSrc,
    o_ALUOp    => s_EXALUOp,
    o_jal      => s_EXjal,
    o_halt     => s_EXhalt
    --o_memRd => EXMemRd
  );

  EX_MEM_Reg : EX_MEM
  PORT MAP(
    i_CLK    => iCLK,
    i_RST    => iRST,
    i_WE     => '1', 
    i_ALU    => s_ALUOut,
    o_ALU    => s_MEMALU,
    i_B      => s_EXB,
    o_B      => s_DMemData,
    i_WrAddr => s_EXrtrd,
    o_WrAddr => s_MEMrtrd,
    i_MemWr  => s_EXMemWr,
    o_MemWr  => s_DMemWr,
    --i_MemRd  => s_EXMemRd,
    --o_MemRd  => s_MEMMemRd,
    i_MemtoReg => s_EXmemToReg,
    o_MemtoReg => s_MEMmemToReg,
    i_Halt => s_EXhalt,
    o_Halt => s_MEMhalt,
    i_RegWr => s_EXRegWr,
        o_RegWr => s_MemRegWr,
        i_jal => s_EXjal,
        o_jal => s_MEMjal,
        i_PC4 => s_EX_PC4,
        o_PC4 => s_MEM_PC4
  );

  MEM_WB_Reg : MEM_WB
  PORT MAP(
    i_CLK    => iCLK,
    i_RST    => iRST,
    i_WE     => '1',
    i_ALU    => s_MEMALU,
    o_ALU    => s_WBALU,
    i_Mem    => s_DMemOut,
    o_Mem    => s_WBMemOut,
    i_WrAddr => s_MEMrtrd,
    o_WrAddr => s_WBrtrd,
    i_MemtoReg => s_MEMmemToReg,
    o_MemtoReg => s_WBmemToReg,
    i_Halt => s_MEMHalt,
    o_Halt => s_Halt,
    i_RegWr => s_MEMRegWr,
        o_RegWr => s_WBRegWr,
        i_jal => s_MEMjal,
        o_jal => s_WBjal,
        i_PC4 => s_MEM_PC4,
        o_PC4 => s_WB_PC4
  );

END structure;
