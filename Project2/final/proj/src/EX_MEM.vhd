LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY EX_MEM IS

    PORT (
        i_CLK      : IN STD_LOGIC;                      -- Clock input
        i_RST      : IN STD_LOGIC;                      -- Reset input
        i_WE       : IN STD_LOGIC;                      -- Write enable input
        i_ALU      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- ALU value input
        o_ALU      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); -- ALU value output
        i_B        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  --Mem address value
        o_B        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Mem address value
        i_WrAddr   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);   --write address to register file
        o_WrAddr   : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
        i_MemWr    : IN STD_LOGIC;   --write to memory
        o_MemWr    : OUT STD_LOGIC;  --write to memory signal
        --i_MemRd    : IN STD_LOGIC;   --read memory
        --o_MemRd    : OUT STD_LOGIC;  --read memory signal
        i_MemtoReg : IN STD_LOGIC;   --memory vs alu signal
        o_MemtoReg : OUT STD_LOGIC;
        i_Halt : in std_logic; --Halt signal
        o_Halt : out std_logic;
        i_RegWr : in std_logic; --write enable for reg file
        o_RegWr : out std_logic;
        i_jal : in std_logic; --jal signal
        o_jal : out std_logic;
        i_PC4 : in std_logic_vector(31 downto 0); --pc+4
        o_PC4 : out std_logic_vector(31 downto 0) 
         ); 
END EX_MEM;

ARCHITECTURE structural OF EX_MEM IS

    COMPONENT dffg IS
        PORT (
            i_CLK : IN STD_LOGIC;   -- Clock input
            i_RST : IN STD_LOGIC;   -- Reset input
            i_WE  : IN STD_LOGIC;   -- Write enable input
            i_D   : IN STD_LOGIC;   -- Data value input
            o_Q   : OUT STD_LOGIC); -- Data value output
    END COMPONENT;

BEGIN

    G_ALU_Reg : FOR i IN 0 TO 31 GENERATE
        ALUDFFGI : dffg PORT MAP(
            i_CLK => i_CLK,
            i_RST => i_RST,
            i_WE  => i_WE,
            i_D   => i_ALU(i),
            o_Q   => o_ALU(i));
    END GENERATE G_ALU_Reg;

    G_B_Reg : FOR i IN 0 TO 31 GENERATE
        BDFFGI : dffg PORT MAP(
            i_CLK => i_CLK,
            i_RST => i_RST,
            i_WE  => i_WE,
            i_D   => i_B(i),
            o_Q   => o_B(i)
        );
    END GENERATE G_B_Reg;

    G_WrAddr_Reg : FOR i IN 0 TO 4 GENERATE
        WrAddrI : dffg PORT MAP(
            i_CLK => i_CLK,
            i_RST => i_RST,
            i_WE  => i_WE,
            i_D   => i_WrAddr(i),
            o_Q   => o_WrAddr(i)
        );
    END GENERATE G_WrAddr_Reg;

    MemWrReg : dffg
    PORT MAP(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => i_WE,
        i_D   => i_MemWr,
        o_Q   => o_MemWr
    );

--    MemRdReg : dffg
--    PORT MAP(
--        i_CLK => i_CLK,
--        i_RST => i_RST,
--        i_WE  => i_WE,
--        i_D   => i_MemRd,
--        o_Q   => o_MemRd
--    );

    MemtoReg : dffg
    PORT MAP(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => i_WE,
        i_D   => i_MemtoReg,
        o_Q   => o_MemtoReg
    );

    HaltReg : dffg
    PORT MAP(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => i_WE,
        i_D   => i_Halt,
        o_Q   => o_Halt
    );

    RegWrReg : dffg
    PORT MAP(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => i_WE,
        i_D   => i_RegWr,
        o_Q   => o_RegWr
    );

    jalReg : dffg
    port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => i_WE,
        i_D   => i_jal,
        o_Q   => o_jal
    );

    G_PC4_reg : FOR i IN 0 TO 31 GENERATE
        WrAddrI : dffg PORT MAP(
            i_CLK => i_CLK,
            i_RST => i_RST,
            i_WE  => i_WE,
            i_D   => i_PC4(i),
            o_Q   => o_PC4(i)
        );
    END GENERATE G_PC4_reg;

END structural;
