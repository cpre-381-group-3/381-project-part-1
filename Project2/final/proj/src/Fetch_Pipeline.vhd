LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Fetch_Pipeline IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
        i_PC4         : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --Program counter
        i_branch_addr : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential branch address
        i_jump_addr   : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential jump address
        i_jr          : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential jump register address
        i_jr_select   : IN STD_LOGIC;                        --selector for jump register
        i_branch      : IN STD_LOGIC;                        --selects if a branch instruction is active
        i_bne         : IN STD_LOGIC;                        --inverts if bne
        --i_zero        : IN STD_LOGIC;                        --if branch, determines if you should branch
        i_jump : IN STD_LOGIC;                         --selector for j or jal
        i_A    : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);  --Reg A value
        i_B    : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);  --Reg B value
        o_PC   : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --program counter output
        --o_PC4  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)  --PC+4 output, used for jal instruction
        o_jump_branch : OUT STD_LOGIC --set to 1 if a branch or jump is taken
    );

END Fetch_Pipeline;

ARCHITECTURE structural OF Fetch_Pipeline IS

    COMPONENT mux2t1_N IS
        PORT (
            i_S  : IN STD_LOGIC;
            i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));
    END COMPONENT;

    COMPONENT Barrel_Shifter IS
        PORT (
            i_data             : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_logic_arithmetic : IN STD_LOGIC;                          -- 0 for logical, 1 for arithmetic (sign bit)
            i_left_right       : IN STD_LOGIC;                          --0 for shift left, 1 for shift right
            i_shamt            : IN STD_LOGIC_VECTOR(4 DOWNTO 0);       --shift amount
            o_Out              : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)); --output of the shifter
    END COMPONENT;

    COMPONENT Full_Adder_N IS
        PORT (
            i_C        : IN STD_LOGIC; --initial carry
            i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_S        : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_C        : OUT STD_LOGIC;
            o_Overflow : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT Zero_Detect IS
        PORT (
            i_F    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_zero : OUT STD_LOGIC);
    END COMPONENT;

    --SIGNAL s_PC4                 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC+4
    SIGNAL s_branch_addr_shifted : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for the branch address after shifted left 2
    SIGNAL s_PC_branch           : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC+4+branchaddr
    SIGNAL s_j_addr_shifted      : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for jump address after shift
    SIGNAL s_PC_j_addr           : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC(31 - 28) + jump address(27 - 0)
    SIGNAL s_PC_or_Branch        : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC+4 or branch address
    SIGNAL s_PC_or_j             : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC or branch or jump
    SIGNAL s_toBranch            : STD_LOGIC;                        --signal to determine if you should branch
    SIGNAL s_bne_branch          : STD_LOGIC;                        --signal if bne is active, should invert the zero
    SIGNAL s_XOR                 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --determines if 0
    SIGNAL s_NOTzero, s_zero     : STD_LOGIC;                        --inverse of 0 detection

BEGIN
    --Find if zero
    PROCESS (i_A, i_B)--or, and, xor, nor
    BEGIN
        FOR i IN 0 TO 31 LOOP
            s_XOR(i) <= i_A(i) XOR i_B(i);
        END LOOP;
    END PROCESS;

    --find the zero
    zero_detector : Zero_Detect
    PORT MAP(
        i_F    => s_XOR,
        o_zero => s_zero);

    s_NOTzero    <= NOT s_zero;
    s_bne_branch <= s_NOTzero XOR i_bne;

        --Shift the branch address by 2
        Branch_Shifter : Barrel_Shifter
        PORT MAP(
            i_data             => i_branch_addr,
            i_logic_arithmetic => '0',     --always logical shift
            i_left_right       => '0',     --left shift
            i_shamt            => "00010", --shift left 2
            o_Out              => s_branch_addr_shifted
        );

    --Add PC+4 + branch address
    Branch_PC_Adder : Full_Adder_N
    PORT MAP(
        i_C => '0', --no carry
        i_A => i_PC4,
        i_B => s_branch_addr_shifted,
        o_S => s_PC_branch
    );

    Jump_Address_Shift : Barrel_Shifter
    PORT MAP(
        i_data             => i_jump_addr,
        i_logic_arithmetic => '0',     --always logical shift
        i_left_right       => '0',     --left shift
        i_shamt            => "00010", --shift left 2
        o_Out              => s_j_addr_shifted
    );

    --Jump Address
    s_PC_j_addr <= i_PC4(31 DOWNTO 28) & s_j_addr_shifted(27 DOWNTO 0);

    s_toBranch <= i_branch AND s_bne_branch;

    Branch_Select : mux2t1_N
    PORT MAP(
        i_S  => s_toBranch,
        i_D0 => i_PC4,
        i_D1 => s_PC_branch,
        o_O  => s_PC_or_Branch
    );

    Jump_Select : mux2t1_N
    PORT MAP(
        i_S  => i_jump,
        i_D0 => s_PC_or_Branch,
        i_D1 => s_PC_j_addr,
        o_O  => s_PC_or_j
    );

    JR_Select : mux2t1_N
    PORT MAP(
        i_S  => i_jr_select,
        i_D0 => s_PC_or_j,
        i_D1 => i_jr,
        o_O  => o_PC
    );

    o_jump_branch <= s_toBranch or i_jump;

END structural;
