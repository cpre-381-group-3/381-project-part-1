library IEEE;
use IEEE.std_logic_1164.all;


entity tb_EX_MEM is
  GENERIC (gCLK_HPER : TIME := 50 ns);
end tb_EX_MEM;

architecture behavior of tb_EX_MEM is
  -- Calculate the clock period as twice the half-period
  CONSTANT cCLK_PER : TIME := gCLK_HPER * 2;
  component EX_MEM
  PORT (
    i_CLK      : IN STD_LOGIC;                      -- Clock input
    i_RST      : IN STD_LOGIC;                      -- Reset input
    i_WE       : IN STD_LOGIC;                      -- Write enable input
    i_ALU      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- ALU value input
    o_ALU      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); -- ALU value output
    i_B        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  --Mem address value
    o_B        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Mem address value
    i_WrAddr   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);   --write address to register file
    o_WrAddr   : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    i_MemWr    : IN STD_LOGIC;   --write to memory
    o_MemWr    : OUT STD_LOGIC;  --write to memory signal
    i_MemRd    : IN STD_LOGIC;   --read memory
    o_MemRd    : OUT STD_LOGIC;  --read memory signal
    i_MemtoReg : IN STD_LOGIC;   --memory vs alu signal
    o_MemtoReg : OUT STD_LOGIC;
    i_Halt : in std_logic; --Halt signal
    o_Halt : out std_logic
     ); 
  end component;

  signal s_CLK : std_logic := '0';
  signal s_i_ALU, s_o_ALU, s_i_B, s_o_B : std_logic_vector(31 downto 0);
  signal s_i_WrAddr, s_o_WrAddr : std_logic_vector(4 downto 0);
  signal s_i_MemWr, s_o_MemWr, s_i_MemRd, s_o_MemRd, s_i_MemtoReg, s_o_MemtoReg, s_i_Halt, s_o_Halt : std_logic;

begin

  DUT: EX_MEM 
  port map(i_CLK => s_CLK,
           i_RST => '0',
           i_WE => '1',
           i_ALU => s_i_ALU,
           o_ALU => s_o_ALU,
           i_B => s_i_B,
           o_B => s_o_B,
           i_WrAddr => s_i_WrAddr,
           o_WrAddr => s_o_WrAddr,
           i_MemWr => s_i_MemWr,
           o_MemWr => s_o_MemWr,
           i_MemRd => s_o_MemRd,
           o_MemRd => s_o_MemRd,
           i_MemtoReg => s_i_MemtoReg,
           o_MemtoReg => s_o_MemtoReg,
           i_Halt => s_i_Halt,
           o_Halt => s_o_Halt
           );

   -- This process sets the clock value (low for gCLK_HPER, then high
    -- for gCLK_HPER). Absent a "wait" command, processes restart 
    -- at the beginning once they have reached the final statement.
    P_CLK : PROCESS
    BEGIN
        s_CLK <= '0';
        WAIT FOR gCLK_HPER;
        s_CLK <= '1';
        WAIT FOR gCLK_HPER;
    END PROCESS;
  -- Testbench process  
  P_TB: process
  begin

    s_i_ALU <= X"00FF00FF";
    s_i_B <= X"0000ABCD";
    s_i_WrAddr <= b"00110";
    s_i_MemWr <= '1';
    s_i_MemRd <= '0';
    s_i_MemtoReg <= '0';
    s_i_Halt <= '0';
    wait for 100 ns; 

    s_i_ALU <= X"ABCDEF12";
    s_i_B <= X"11111111";
    s_i_WrAddr <= b"11111";
    s_i_MemWr <= '0';
    s_i_MemRd <= '1';
    s_i_MemtoReg <= '1';
    s_i_Halt <= '1';
    wait for 100 ns; 


    wait;
  end process;
  
end behavior;