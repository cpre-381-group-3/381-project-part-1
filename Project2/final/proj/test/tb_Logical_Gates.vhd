LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Logical_Gates IS
generic(N : integer := 32);
END tb_Logical_Gates;

architecture tb of tb_Logical_Gates is
     
    component Logical_Gates
    port(
        i_A         : in std_logic_vector(N-1 downto 0);
       i_B         : in std_logic_vector(N-1 downto 0);
       o_OR, o_AND, o_XOR, o_NOR          : out std_logic_vector(N-1 downto 0)
       );
    end component;

    signal s_A, s_B, s_OR, s_AND, s_XOR,s_NOR : std_logic_vector(N-1 downto 0);

    begin
        DUT : Logical_Gates
        port map(
            i_A => s_A,
            i_B => s_B,
            o_OR => s_OR,
            o_AND => s_ANd,
            o_XOR => s_XOR,
            o_NOR => s_NOR);


    -- Testbench process  
    P_TB : PROCESS
    BEGIN


        --All F's
        s_A <= x"FFFFFFFF";
        s_B <= x"FFFFFFFF";
        wait for 100 ns;


        --All 0's
        s_A <= x"00000000";
        s_B <= x"00000000";
        wait for 100 ns;


        --Alternate A
        s_A <= b"10101010101010101010101010101010";
        s_B <= b"00000000000000000000000000000000";
        wait for 100 ns;


        --Alternate B
        s_A <= b"00000000000000000000000000000000";
        s_B <= b"10101010101010101010101010101010";
        wait for 100 ns;

       



    end process;
    end tb;

