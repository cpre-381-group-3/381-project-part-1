library IEEE;
use IEEE.std_logic_1164.all;


entity tb_IF_ID_reg is
  GENERIC (gCLK_HPER : TIME := 50 ns);
end tb_IF_ID_reg;

architecture behavior of tb_IF_ID_reg is
  -- Calculate the clock period as twice the half-period
  CONSTANT cCLK_PER : TIME := gCLK_HPER * 2;
  component IF_ID_reg
	port(i_CLK		: in std_logic;
	     i_RST		: in std_logic;
	     i_PC4		: in std_logic_vector(31 downto 0);
	     i_instruction  	: in std_logic_vector(31 downto 0);
	     o_PC4	  	: out std_logic_vector(31 downto 0);
	     o_instruction	: out std_logic_vector(31 downto 0));
  end component;

  signal s_CLK : std_logic := '0';
  signal s_i_PC4     : std_logic_vector(31 downto 0) := (others=> '0');
  signal s_i_instruction     : std_logic_vector(31 downto 0) := (others=> '0');
  signal s_o_PC4     : std_logic_vector(31 downto 0);
  signal s_o_instruction     : std_logic_vector(31 downto 0);

begin

  DUT: IF_ID_reg 
  port map(i_CLK => s_CLK,
           i_RST => '0',
           i_PC4   => s_i_PC4,
           i_instruction   => s_i_instruction,
           o_PC4   => s_o_PC4,
	   o_instruction => s_o_instruction);

   -- This process sets the clock value (low for gCLK_HPER, then high
    -- for gCLK_HPER). Absent a "wait" command, processes restart 
    -- at the beginning once they have reached the final statement.
    P_CLK : PROCESS
    BEGIN
        s_CLK <= '0';
        WAIT FOR gCLK_HPER;
        s_CLK <= '1';
        WAIT FOR gCLK_HPER;
    END PROCESS;
  -- Testbench process  
  P_TB: process
  begin

    s_i_PC4 <= X"EEEEEEEE";
    s_i_instruction <= X"EE0EE0EE"; -- shift by 10
    wait for 100 ns; 

    s_i_PC4 <= X"01001091";
    s_i_instruction <= X"1022FF00"; -- shift by 10
    wait for 100 ns; 


    wait;
  end process;
  
end behavior;