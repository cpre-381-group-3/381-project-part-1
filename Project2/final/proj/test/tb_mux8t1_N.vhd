LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_mux8t1_N IS
    GENERIC (N : INTEGER := 32);
END tb_mux8t1_N;

ARCHITECTURE tb OF tb_mux8t1_N IS
    COMPONENT mux8t1_N
        PORT (
            i_S  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D2 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D3 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D4 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D5 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D6 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D7 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)
        );
    END COMPONENT;

    SIGNAL s_S                                                 : STD_LOGIC_VECTOR(2 DOWNTO 0);
    SIGNAL s_D0, s_D1, s_D2, s_D3, s_D4, s_D5, s_D6, s_D7, s_O : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);

BEGIN
    DUT : mux8t1_N
    PORT MAP(
        i_S  => s_S,
        i_D0 => s_D0,
        i_D1 => s_D1,
        i_D2 => s_D2,
        i_D3 => s_D3,
        i_D4 => s_D4,
        i_D5 => s_D5,
        i_D6 => s_D6,
        i_D7 => s_D7,
        o_O  => s_O);

    -- Testbench process  
    P_TB : PROCESS
    BEGIN

        --Select Data From i_D0
        s_S  <= b"000";
        s_D0 <= x"F00DF00D";
        s_D1 <= x"00000000";
        s_D2 <= x"00000000";
        s_D3 <= x"00000000";
        s_D4 <= x"00000000";
        s_D5 <= x"00000000";
        s_D6 <= x"00000000";
        s_D7 <= x"00000000";
        wait for 100 ns;

        --Select Data From i_D3
        s_S  <= b"011";
        s_D0 <= x"00000000";
        s_D1 <= x"00000000";
        s_D2 <= x"00000000";
        s_D3 <= x"FF00FF00";
        s_D4 <= x"00000000";
        s_D5 <= x"00000000";
        s_D6 <= x"00000000";
        s_D7 <= x"00000000";
        wait for 100 ns;

        --Select Data From i_D7
        s_S  <= b"111";
        s_D0 <= x"F00DF00D";
        s_D1 <= x"00000000";
        s_D2 <= x"00000000";
        s_D3 <= x"00000000";
        s_D4 <= x"00000000";
        s_D5 <= x"00000000";
        s_D6 <= x"00000000";
        s_D7 <= x"ABCDEF00";
        wait for 100 ns;

    END PROCESS;
END tb;