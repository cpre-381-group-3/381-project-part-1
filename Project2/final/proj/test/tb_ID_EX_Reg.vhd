library IEEE;
use IEEE.std_logic_1164.all;


entity tb_ID_EX_Reg is
  GENERIC (gCLK_HPER : TIME := 50 ns);
end tb_ID_EX_Reg;

architecture behavior of tb_ID_EX_Reg is
  -- Calculate the clock period as twice the half-period
  CONSTANT cCLK_PER : TIME := gCLK_HPER * 2;
  component ID_EX_Reg
	port(i_CLK            : in std_logic;
    i_RST                 : in std_logic;
    i_PC4                 : in std_logic_vector(31 downto 0);
    i_readA               : in std_logic_vector(31 downto 0);
    i_readB               : in std_logic_vector(31 downto 0);
    i_signExtImmed        : in std_logic_vector(31 downto 0);
    i_JA                  : in std_logic_vector(31 downto 0); --jump address
    i_inst20_16           : in std_logic_vector(4 downto 0);
    i_inst15_11           : in std_logic_vector(4 downto 0);
    i_control             : in std_logic_vector(14 downto 0);
    o_PC4                 : out std_logic_vector(31 downto 0);
    o_readA               : out std_logic_vector(31 downto 0);
    o_readB               : out std_logic_vector(31 downto 0);
    o_signExtImmed        : out std_logic_vector(31 downto 0);
    o_JA                  : out std_logic_vector(31 downto 0); --jump address
    o_inst20_16           : out std_logic_vector(4 downto 0);
    o_inst15_11           : out std_logic_vector(4 downto 0);
    o_control             : out std_logic_vector(14 downto 0));
  end component;

  signal s_CLK : std_logic := '0';
  signal s_i_PC4, s_i_readA, s_i_readB, s_i_signExtImmed, s_i_JA      :  std_logic_vector(31 downto 0) := (others=> '0');     
  signal s_i_inst20_16, s_i_inst15_11                                 :  std_logic_vector(4 downto 0) := (others=> '0');  
  signal s_i_control                                                  :  std_logic_vector(14 downto 0) := (others=> '0');  
  signal s_o_PC4, s_o_readA, s_o_readB, s_o_signExtImmed, s_o_JA      :  std_logic_vector(31 downto 0);     
  signal s_o_inst20_16, s_o_inst15_11                                 :  std_logic_vector(4 downto 0);  
  signal s_o_control                                                  :  std_logic_vector(14 downto 0); 

begin

  DUT: ID_EX_Reg 
  port map(i_CLK => s_CLK,
           i_RST => '0',
           i_PC4 => s_i_PC4,       
           i_readA => s_i_readA,      
           i_readB => s_i_readB,      
           i_signExtImmed => s_i_signExtImmed,
           i_JA => s_i_JA,         
           i_inst20_16 => s_i_inst20_16,   
           i_inst15_11 => s_i_inst15_11,  
           i_control => s_i_control,     
           o_PC4 => s_o_PC4,         
           o_readA => s_o_readA,       
           o_readB => s_o_readB,       
           o_signExtImmed => s_o_signExtImmed,
           o_JA => s_o_JA,       
           o_inst20_16 => s_o_inst20_16,  
           o_inst15_11 => s_o_inst15_11,   
           o_control => s_o_control);

   -- This process sets the clock value (low for gCLK_HPER, then high
    -- for gCLK_HPER). Absent a "wait" command, processes restart 
    -- at the beginning once they have reached the final statement.
    P_CLK : PROCESS
    BEGIN
        s_CLK <= '0';
        WAIT FOR gCLK_HPER;
        s_CLK <= '1';
        WAIT FOR gCLK_HPER;
    END PROCESS;
  -- Testbench process  
  P_TB: process
  begin

    s_i_PC4 <= X"EEEEEEEE";
    s_i_readA <= X"AAAAAAAA";
    s_i_readB <= X"BBBBBBBB";
    s_i_signExtImmed <= X"CCCCCCCC";
    s_i_JA <= X"DDDDDDDD";
    s_i_inst20_16 <= "11111";
    s_i_inst15_11 <= "00000";
    s_i_control <= "111111111111111";
    wait for 100 ns; 


    wait;
  end process;
  
end behavior;