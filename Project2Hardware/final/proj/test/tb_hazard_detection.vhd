library IEEE;
use IEEE.std_logic_1164.all;


entity tb_hazard_detection is
  GENERIC (gCLK_HPER : TIME := 50 ns);
end tb_hazard_detection;

architecture behavior of tb_hazard_detection is
  -- Calculate the clock period as twice the half-period
  CONSTANT cCLK_PER : TIME := gCLK_HPER * 2;
  component hazard_detection
	port(   i_jump_ID          : in std_logic; --Control Hazard
            i_jump_EX          : in std_logic; --Control Hazard
            i_jump_MEM         : in std_logic; --Control Hazard
            i_jump_WB          : in std_logic; --Control Hazard
            i_branch_ID        : in std_logic; --Control Hazard
            i_branch_EX        : in std_logic; --Control Hazard
            i_branch_MEM       : in std_logic; --Control Hazard
            i_branch_WB        : in std_logic; --Control Hazard
    
            i_rAddrA 	  	: in std_logic_vector(4 downto 0); --Data Hazard
            i_rAddrB	  	    : in std_logic_vector(4 downto 0); --Data Hazard
            i_wAddr_ID      : in std_logic_vector(4 downto 0); -- Write Address for ID
            i_wAddr_EX      : in std_logic_vector(4 downto 0); -- Write Address for MEM
            i_wE_ID    : in std_logic; -- Write enable for ID
            i_wE_EX    : in std_logic; -- Write enable for MEM
    
            o_stall    		: out std_logic); -- 1 if stalling is true
  end component;

 signal s_i_jump_ID : std_logic;
 signal s_i_jump_EX : std_logic;
 signal s_i_jump_MEM : std_logic;
 signal s_i_jump_WB : std_logic;
 signal s_i_branch_ID : std_logic;
 signal s_i_branch_EX : std_logic;
 signal s_i_branch_MEM : std_logic;
 signal s_i_branch_WB : std_logic;
 signal s_i_rAddrA : std_logic_vector(4 downto 0);
 signal s_i_rAddrB : std_logic_vector(4 downto 0);
 signal s_i_wAddr_ID : std_logic_vector(4 downto 0);
 signal s_i_wAddr_EX : std_logic_vector(4 downto 0);
 signal s_i_wE_ID : std_logic;
 signal s_i_wE_EX : std_logic;
 signal s_o_stall : std_logic;



begin

  DUT: hazard_detection
  port map(
    i_jump_ID => s_i_jump_ID,
    i_jump_EX => s_i_jump_EX,
    i_jump_MEM => s_i_jump_MEM,
    i_jump_WB => s_i_jump_WB,
    i_branch_ID => s_i_branch_ID,
    i_branch_EX => s_i_branch_EX,
    i_branch_MEM => s_i_branch_MEM, 
    i_branch_WB => s_i_branch_WB,
    i_rAddrA => s_i_rAddrA,
    i_rAddrB => s_i_rAddrB,
    i_wAddr_ID => s_i_wAddr_ID,
    i_wAddr_EX => s_i_wAddr_EX,
    i_wE_ID => s_i_wE_ID,
    i_wE_EX => s_i_wE_EX,
    o_stall => s_o_stall);

 
  -- Testbench process  run 600
  P_TB: process
  begin

    s_i_wE_ID <= '1';
    s_i_rAddrA <= "10000";
    s_i_wAddr_ID <= "10000";
    wait for 100 ns; -- stall should be 1, tests first half of first if

    s_i_wE_ID <= '0'; -- used to make first half of first if false 
    s_i_wE_EX <= '1';
    s_i_wAddr_EX <= "10000"; -- Dont need ti edut rAddrA again
    wait for 100 ns; -- stall should be 1, tests second half of first if

    s_i_rAddrA <= "00000"; -- Makes first if false
    s_i_wE_ID <= '1';
    s_i_wE_EX <= '0'; -- used to make second half of second if false 
    s_i_rAddrB <= "10000";
    wait for 100 ns; -- stall should be 1, tests first half of second if

    s_i_wE_ID <= '0'; -- used to make first half of second if false 
    s_i_wE_EX <= '1';
    wait for 100 ns; -- stall should be 1, tests second half of second if

    s_i_wE_ID <= '0'; -- used to make if statements false
    s_i_wE_EX <= '0';
    s_i_jump_ID <= '1';
    wait for 100 ns; -- stall should be 1

    s_i_jump_ID <= '0';
    s_i_branch_ID <= '1';
    wait for 100 ns; -- stall should be 1

 



    wait;
  end process;
  
end behavior;