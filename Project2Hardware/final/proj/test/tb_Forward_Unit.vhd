LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Forward_Unit IS
END tb_Forward_Unit;

ARCHITECTURE tb OF tb_Forward_Unit IS
    CONSTANT N : INTEGER := 32;
    COMPONENT Forward_Unit
        PORT (
            i_EX_rs : in std_logic_vector(4 downto 0);
        i_EX_rt : in std_logic_vector(4 downto 0);
        i_MEM_rd : in std_logic_vector(4 downto 0);
        i_WB_rd : in std_logic_vector(4 downto 0);
        i_MEM_wb : in std_logic;
        i_WB_wb : in std_logic;
        o_Forward_A : out std_logic_vector(1 downto 0);
        o_Forward_B : out std_logic_vector(1 downto 0)
        );
    END COMPONENT;

    -- Temporary signals to connect to the component.
    SIGNAL s_MEM_wb, s_WB_wb : STD_LOGIC;
    SIGNAL s_EX_rs, s_EX_rt, s_MEM_rd, s_WB_rd                          : STD_LOGIC_VECTOR(4 DOWNTO 0);
    SIGNAL s_Forward_A, s_Forward_B                    : STD_LOGIC_VECTOR(1 DOWNTO 0);

BEGIN

    DUT : Forward_Unit
    PORT MAP(
        i_EX_rs => s_EX_rs,
        i_EX_rt => s_EX_rt,
        i_MEM_rd => s_MEM_rd,
        i_WB_rd => s_WB_rd,
        i_MEM_wb => s_MEM_wb,
        i_WB_wb => s_WB_wb,
        o_Forward_A => s_Forward_A,
        o_Forward_B => s_Forward_B);

    -- Testbench process  
    P_TB : PROCESS
    BEGIN

        --test no forward, output 00
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00010";
        s_MEM_rd <= b"10000";
        s_WB_rd <= b"01000";
        s_MEM_wb <= '1';
        s_WB_wb <= '1';
        WAIT FOR 100 ns;

        --test no forward, different case
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00010";
        s_MEM_rd <= b"10000";
        s_WB_rd <= b"01000";
        s_MEM_wb <= '0';
        s_WB_wb <= '0';
        WAIT FOR 100 ns;

        --test no forward, zero rd
        s_EX_rs <= b"00000";
        s_EX_rt <= b"00000";
        s_MEM_rd <= b"00000";
        s_WB_rd <= b"00000";
        s_MEM_wb <= '1';
        s_WB_wb <= '1';
        WAIT FOR 100 ns;

        --test no forward because no wb, output 00
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00001";
        s_MEM_rd <= b"00001";
        s_WB_rd <= b"00001";
        s_MEM_wb <= '0';
        s_WB_wb <= '0';
        WAIT FOR 100 ns;

        --test forwardA from MEM state, output A = 10
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00010";
        s_MEM_rd <= b"00001";
        s_WB_rd <= b"01000";
        s_MEM_wb <= '1';
        s_WB_wb <= '0';
        WAIT FOR 100 ns;

        --same but for B
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00010";
        s_MEM_rd <= b"00010";
        s_WB_rd <= b"01000";
        s_MEM_wb <= '1';
        s_WB_wb <= '1';
        WAIT FOR 100 ns;

        --test wb forward for A output A = 01
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00010";
        s_MEM_rd <= b"10000";
        s_WB_rd <= b"00001";
        s_MEM_wb <= '0';
        s_WB_wb <= '1';
        WAIT FOR 100 ns;

        --same but for b
        s_EX_rs <= b"00001";
        s_EX_rt <= b"00010";
        s_MEM_rd <= b"10000";
        s_WB_rd <= b"00010";
        s_MEM_wb <= '1';
        s_WB_wb <= '1';
        WAIT FOR 100 ns;

        --test both forward cases are met, should forward from MEM, 10 for both
        s_EX_rs <= b"00100";
        s_EX_rt <= b"00100";
        s_MEM_rd <= b"00100";
        s_WB_rd <= b"00100";
        s_MEM_wb <= '1';
        s_WB_wb <= '1';
        WAIT FOR 100 ns;

    END PROCESS;
END tb;