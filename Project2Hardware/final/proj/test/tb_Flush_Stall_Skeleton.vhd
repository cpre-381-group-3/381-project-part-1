-- tb_Flush_Stall_Skeleton.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the tb of the IF_ID_Reg --> ID_EX_Reg 
-- --> EX_MEM_Reg --> MEM_WB_Reg registers in the pipelined processor. It is the 
-- compenent that will test if flushing and stalling is working correctly.
-------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Flush_Stall_Skeleton IS
    GENERIC (gCLK_HPER : TIME := 50 ns);
END tb_Flush_Stall_Skeleton;

ARCHITECTURE tb OF tb_Flush_Stall_Skeleton IS
    -- Calculate the clock period as twice the half-period
    CONSTANT cCLK_PER : TIME := gCLK_HPER * 2;
    COMPONENT Flush_Stall_Skeleton IS
        PORT (
            i_CLK              : IN STD_LOGIC;
            i_RST              : IN STD_LOGIC;
            i_stall_IFID       : IN STD_LOGIC;
            i_stall_IDEX       : IN STD_LOGIC;
            i_stall_EXMEM      : IN STD_LOGIC;
            i_stall_MEMWB      : IN STD_LOGIC;
            i_PC4              : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            i_instruction      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_IFID_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_IDEX_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_EXMEM_PC4        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_MEMWB_PC4        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_IFID_instruction : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    END COMPONENT;

    --Temporary signals to control to the component
    SIGNAL s_Clk, s_RST, s_stall_IFID, s_stall_IDEX, s_stall_EXMEM, s_stall_MEMWB                     : STD_LOGIC;
    SIGNAL s_PC4, s_instruction, s_IFID_PC4, s_IDEX_PC4, s_EXMEM_PC4, s_MEMWB_PC4, s_IFID_instruction : STD_LOGIC_VECTOR(31 DOWNTO 0);

BEGIN

    DUT : Flush_Stall_Skeleton
    PORT MAP(
        i_CLK              => s_CLK,
        i_RST              => s_RST,
        i_stall_IFID       => s_stall_IFID,
        i_stall_IDEX       => s_stall_IDEX,
        i_stall_EXMEM      => s_stall_EXMEM,
        i_stall_MEMWB      => s_stall_MEMWB,
        i_PC4              => s_PC4,
        i_instruction      => s_instruction,
        o_IFID_PC4         => s_IFID_PC4,
        o_IDEX_PC4         => s_IDEX_PC4,
        o_EXMEM_PC4        => s_EXMEM_PC4,
        o_MEMWB_PC4        => s_MEMWB_PC4,
        o_IFID_instruction => s_IFID_instruction
    );

    -- This process sets the clock value (low for gCLK_HPER, then high
    -- for gCLK_HPER). Absent a "wait" command, processes restart 
    -- at the beginning once they have reached the final statement.
    P_CLK : PROCESS
    BEGIN
        s_CLK <= '0';
        WAIT FOR gCLK_HPER;
        s_CLK <= '1';
        WAIT FOR gCLK_HPER;
    END PROCESS;
    -- Testbench process  
    P_TB : PROCESS
    BEGIN

        --Register movement

        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000000"; --PC+4 = 0
        s_instruction <= x"11111111"; --instruction 1
        WAIT FOR 100 ns;

        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000004"; --PC+4 = 4
        s_instruction <= x"22222222"; --instruction 2
        WAIT FOR 100 ns;

        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000008"; --PC+4 = 8
        s_instruction <= x"33333333"; --instruction 3
        WAIT FOR 100 ns;

        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"0000000C"; --PC+4 = 8
        s_instruction <= x"44444444"; --instruction 4
        WAIT FOR 100 ns;
        --Register movement end

        -- Stall MEMWB
        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '1';         --MEMWB Stall
        s_PC4         <= x"00000010"; --PC+4 = 16
        s_instruction <= x"55555555"; --instruction 5
        WAIT FOR 100 ns;

        -- Stall EXMEM
        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '1';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000014"; --PC+4 = 20
        s_instruction <= x"66666666"; --instruction 6
        WAIT FOR 100 ns;

        -- Stall IDEX
        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '1';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000018"; --PC+4 = 24
        s_instruction <= x"77777777"; --instruction 7
        WAIT FOR 100 ns;

        -- Stall IDEX
        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '1';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"0000001c"; --PC+4 = 28
        s_instruction <= x"88888888"; --instruction 8
        WAIT FOR 100 ns;


        --resume
        s_RST         <= '0';         --not flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000020"; --PC+4 = 32
        s_instruction <= x"99999999"; --instruction 9
        WAIT FOR 100 ns;


        --flush that thang
        s_RST         <= '1';         --flushing
        s_stall_IFID  <= '0';         --IFID Stall
        s_stall_IDEX  <= '0';         --IDEX Stall
        s_stall_EXMEM <= '0';         --EXMEM Stall
        s_stall_MEMWB <= '0';         --MEMWB Stall
        s_PC4         <= x"00000024"; --PC+4 = 36
        s_instruction <= x"10101010"; --instruction 10
        WAIT FOR 100 ns;
        WAIT;
    END PROCESS;

END tb;