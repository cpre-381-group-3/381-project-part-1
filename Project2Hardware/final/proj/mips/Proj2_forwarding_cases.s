.data

data1: .word 0

.text
.globl main

main:
	lw $t0, data1
	addi $t2, $0, 1
	sw $t2, data1
	
	
	#--case 1 Reg A WB Mem Hazard
           # if i_WB_wb = '1'      and       i_WB_rd = i_EX_rs      and        i_WB_rd /= "00000"
           
           lw $t2, data1
           nop #control hazard if instruction is here which we aren't testing
          add $t3, $t2, $t1
           
           
           
           
        #--case 2 Reg B WB Mem Hazard
          #  if i_WB_wb = '1'    and        i_WB_rd = i_EX_rt       and         i_WB_rd /= "00000"
	
           
           lw $t2, data1
           nop #control hazard if instruction is here which we aren't testing
           add $t3, $t1, $t2
           
           
           
           
          
	#--case 3 Reg A Execute back to mem state hazard
           # if i_MEM_wb = '1'  and        i_MEM_rd = i_EX_rs       and          i_MEM_rd /= "00000"
           
           
          lw $t1, data1
	add $t2, $t1, $0
	
           
           
        #--case 4 Reg B Execute back to mem state hazard
        #  if i_MEM_wb = '1'  and         i_MEM_rd = i_EX_rt        and          i_MEM_rd /= "00000"
        
         lw $t1, data1
	add $t2, $t0, $1
	
	j exit
exit:
	halt

