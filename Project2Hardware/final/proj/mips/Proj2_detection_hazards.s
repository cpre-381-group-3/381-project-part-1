main:
	#RegReadA Hazard
	#((i_wE_ID = '1'     AND      i_rAddrA = i_wAddr_ID        AND      i_rAddrA /= "00000") OR 
	#(i_wE_EX = '1'      AND      i_rAddrA = i_wAddr_EX        AND      i_rAddrA /= "00000"))
	#RegReadB Hazard
	#((i_wE_ID = '1'     AND      i_rAddrB = i_wAddr_ID        AND      i_rAddrB /= "00000") OR 
	#(i_wE_EX = '1'      AND      i_rAddrB = i_wAddr_EX        AND      i_rAddrB /= "00000"))
	
	addi $t0, $zero, 1 #add 1 to t0
	add $t1, $t0, $t0 #t1 = t1 + t1 = 2
	add $t2, $t1, $t1 #t2 = t1 + t1 = 4
	add $t3, $t2, $t2 #t3 = t2 + t2 = 8
	add $t4, $t3, $t3 #t4 = t3 + t3 = 16
	
	
	
	#Control Hazard
	#i_jump_ID='1'        OR        i_branch_ID = '1')
	j jumpHazard
	addi $t3, $zero, 1 #This should not be executed on our processor
jumpHazard:
	addi $t1, $zero, 1 #set $t1 to 1
	beq $t0, $t1, branchHazard
	addi $t3, $zero, 1 #This should not be executed on our processor
branchHazard:
	j exit
exit:
	halt

