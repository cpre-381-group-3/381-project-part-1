.data
    
    data:   .word   4, 2, 10, 8, 6, 1, 3, 9, 7, 5
    size:   .word   10

.text
main:

    lw $t2, size # $t2 = size = 10
    add $t0, $zero, $zero #i = $t0 = 0
    la $s0, data  #load the base address of the data array into $s0
    subu $t2, $t2, 1 #size = size-1




    loop_i:
    	nop
    addi $t3, $t2, 0 #copy size to $t3
    slt $at, $t0, $t2 #sets $at to 1 if i<size-1    
    bne $at, $zero, loop_i_continue #moves into i loop if i<size-1
    	nop
   	nop
    j exit_loop_i #i>size-1
    	nop
    	nop
    	nop
   
    loop_i_continue:
    sub $t3, $t3, $t0 #size = size-1-i
    add $t1, $zero, $zero #j = $t1 = 0
   	
    	loop_j:
    	nop
     	slt $at, $t1, $t3 #sets $at to 1 if j<size-1-i
     	bne $at, $zero, loop_j_continue #moves into j loop if j<size-1-i
     		nop
     		nop
     	j exit_loop_j #j>=size-1-i
     		nop
     		nop
     		nop
     
     
   		 loop_j_continue:
   		 #swap function
  		 sll $t4, $t1, 2 #j offset in $t4
  		 	nop
  		 	nop
  		 add $t5, $t4, 4 #j+1 offset in $t5
  		 add $t6, $t4, $s0 #address of j data
  		 	nop
  		 add $t7, $t5, $s0 #addresss of j+1 data
   			
  		 lw $t8, 0($t6) #j data
  		 	nop
  		 lw $t9, 0($t7) #j+1 data
   			nop
   			nop
   		 slt $at, $t9, $t8 #is j+1 < j
  		 bne $at, $zero, swap
  		 	nop
  		 	nop
  		 
   		 addi $t1, $t1, 1 #j=j+1
   		 j loop_j
   		 	nop
   		 	nop
   		 	nop
    
   			swap: 
   			sw $t9, 0($t6) #j+1 data into memory at address j
  			 sw $t8, 0($t7) #j data into memory at address j+1
   			addi $t1, $t1, 1 #j=j+1
   			j loop_j
   				nop
   				nop
   				
   

   
    	exit_loop_j:
    	add $t1, $zero, $zero #j back to 0
   	addi $t0, $t0, 1 #i+1 ------------------------Where it normally goes.......
   	
    	j loop_i
    	#addi $t0, $t0, 1 #i+1 -------------------------Big Change Here for control flow hazard (Should work on our processor)
    		nop
    		nop
    		
   
   
    exit_loop_i:
    li $v0, 10 #end program
    syscall
halt


