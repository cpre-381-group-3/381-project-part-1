LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY mux4t1 IS
  PORT (
    i_S  : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    i_D0 : IN STD_LOGIC;
    i_D1 : IN STD_LOGIC;
    i_D2 : IN STD_LOGIC;
    i_D3 : IN STD_LOGIC;
    o_O  : OUT STD_LOGIC);

END mux4t1;

architecture dataflow of mux4t1 is
  begin
  o_O <= i_D0 when (i_S = "00") else
  i_D1 when (i_S = "01") else
  i_D2 when (i_S = "10") else
  i_D3 when (i_S = "11") else
  '0';
  end dataflow;
