-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- mux2t1_N.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of an N-bit wide 2:1
-- mux using structural VHDL, generics, and generate statements.
--
--
-- NOTES:
-- 1/6/20 by H3::Created.

--This is built off of the framework provided, as specified in the lab instructions
--Added to by Cael Schreier
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2t1 is
  port(i_S          : in std_logic;
       i_D0         : in std_logic;
       i_D1         : in std_logic;
       o_O          : out std_logic);

end mux2t1;

architecture structural of mux2t1 is

  component invg is
    port(i_A                  : in std_logic;
         o_F                : out std_logic);
  end component;

  component andg2 is
    port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
  end component;

  component org2 is
    port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
  end component;  

  --signals for stored values
  signal n1     : std_logic; --not s
  signal a1      : std_logic; --first and
  signal a2     : std_logic; --second and

begin

  ---------------------------------------------------------------------------
  -- Not s
  ---------------------------------------------------------------------------
 
  not1: invg
    port MAP(i_A    => i_s,
    o_F               => n1);
  ---------------------------------------------------------------------------
  -- And1
  ---------------------------------------------------------------------------

  and1: andg2
  port MAP(i_A    => i_d0,
  i_B               => n1,
  o_F               => a1);

  ---------------------------------------------------------------------------
  -- And2
  ---------------------------------------------------------------------------

  and2: andg2
  port MAP(i_A    => i_d1,
  i_B               => i_s,
  o_F               => a2);

  ---------------------------------------------------------------------------
  -- Final OR
  ---------------------------------------------------------------------------

  or1: org2
  port MAP(i_A    => a1,
  i_B               => a2,
  o_F               => o_O);


  
end structural;
