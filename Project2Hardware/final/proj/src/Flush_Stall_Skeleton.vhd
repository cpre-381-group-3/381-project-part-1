-- Flush_Stall_Skeleton.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of IF_ID_Reg --> ID_EX_Reg 
-- --> EX_MEM_Reg --> MEM_WB_Reg registers in the pipelined processor. It is the 
-- compenent that will test if flushing and stalling is working correctly.
-------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
ENTITY Flush_Stall_Skeleton IS
    PORT (
        i_CLK              : IN STD_LOGIC;
        i_RST              : IN STD_LOGIC;
        i_stall_IFID       : IN STD_LOGIC;
        i_stall_IDEX       : IN STD_LOGIC;
        i_stall_EXMEM      : IN STD_LOGIC;
        i_stall_MEMWB      : IN STD_LOGIC;
        i_PC4              : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        i_instruction      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        o_IFID_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        o_IDEX_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        o_EXMEM_PC4        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        o_MEMWB_PC4        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        o_IFID_instruction : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));

END Flush_Stall_Skeleton;

ARCHITECTURE structural OF Flush_Stall_Skeleton IS

    COMPONENT IF_ID_Reg IS
        PORT (
            i_CLK         : IN STD_LOGIC;
            i_RST         : IN STD_LOGIC;
            i_stall       : IN STD_LOGIC;
            i_PC4         : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            i_instruction : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_instruction : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    END COMPONENT;

    COMPONENT ID_EX_Reg IS
        PORT (
            i_CLK   : IN STD_LOGIC;
            i_RST   : IN STD_LOGIC;
            i_stall : IN STD_LOGIC; --stall control

            i_PC4          : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            i_readA        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            i_readB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            i_signExtImmed : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            --i_JA           : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
            i_inst20_16 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_inst15_11 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_RegDst    : IN STD_LOGIC;
            i_RegWrite  : IN STD_LOGIC;
            i_memToReg  : IN STD_LOGIC;
            i_MemWrite  : IN STD_LOGIC;
            i_ALUSrc    : IN STD_LOGIC;
            i_ALUOp     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            i_jal       : IN STD_LOGIC;
            i_halt      : IN STD_LOGIC;
            --i_memRd        : in std_logic;
            --i_control             : in std_logic_vector(14 downto 0);
            o_PC4          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_readA        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_readB        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_signExtImmed : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            --o_JA           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
            o_inst20_16 : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
            o_inst15_11 : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
            --o_control             : out std_logic_vector(14 downto 0));
            o_RegDst   : OUT STD_LOGIC;
            o_RegWrite : OUT STD_LOGIC;
            o_memToReg : OUT STD_LOGIC;
            o_MemWrite : OUT STD_LOGIC;
            o_ALUSrc   : OUT STD_LOGIC;
            o_ALUOp    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            o_jal      : OUT STD_LOGIC;
            o_halt     : OUT STD_LOGIC
            --o_memRd : out std_logic
        );
    END COMPONENT;

    COMPONENT EX_MEM IS
        PORT (
            i_CLK : IN STD_LOGIC; -- Clock input
            i_RST : IN STD_LOGIC; -- Reset input
            --i_WE     : IN STD_LOGIC;                      -- Write enable input
            i_stall  : IN STD_LOGIC;                      --Stall input
            i_ALU    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- ALU value input
            o_ALU    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); -- ALU value output
            i_B      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  --Mem address value
            o_B      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Mem address value
            i_WrAddr : IN STD_LOGIC_VECTOR(4 DOWNTO 0);   --write address to register file
            o_WrAddr : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_MemWr  : IN STD_LOGIC;  --write to memory
            o_MemWr  : OUT STD_LOGIC; --write to memory signal
            --i_MemRd    : IN STD_LOGIC;   --read memory
            --o_MemRd    : OUT STD_LOGIC;  --read memory signal
            i_MemtoReg : IN STD_LOGIC; --memory vs alu signal
            o_MemtoReg : OUT STD_LOGIC;
            i_Halt     : IN STD_LOGIC; --Halt signal
            o_Halt     : OUT STD_LOGIC;
            i_RegWr    : IN STD_LOGIC; --write enable for reg file
            o_RegWr    : OUT STD_LOGIC;
            i_PC4      : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --pc+4
            o_PC4      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT MEM_WB IS
        PORT (
            i_CLK : IN STD_LOGIC; -- Clock input
            i_RST : IN STD_LOGIC; -- Reset input
            -- i_WE       : IN STD_LOGIC;                      -- Write enable input
            i_stall    : IN STD_LOGIC;                      --Stall input 
            i_ALU      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- ALU value input
            o_ALU      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); -- ALU value output
            i_Mem      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  --Mem value
            o_Mem      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Mem value
            i_WrAddr   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);   --write address to register file
            o_WrAddr   : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_MemtoReg : IN STD_LOGIC; --memory vs alu signal
            o_MemtoReg : OUT STD_LOGIC;
            i_Halt     : IN STD_LOGIC; --halt signal
            o_Halt     : OUT STD_LOGIC;
            i_RegWr    : IN STD_LOGIC; --write enable for reg file
            o_RegWr    : OUT STD_LOGIC;
            i_PC4      : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --pc+4
            o_PC4      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
        );
    END COMPONENT;

    SIGNAL s_IFID_PC4                           : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL s_IDEX_PC4, s_EXMEM_PC4, s_MEMWB_PC4 : STD_LOGIC_VECTOR(31 DOWNTO 0);
BEGIN
    IF_ID : IF_ID_Reg PORT MAP(
        i_CLK         => i_CLK,
        i_RST         => i_RST,
        i_stall       => i_stall_IFID,
        i_PC4         => i_PC4,
        i_instruction => i_instruction,
        o_PC4         => s_IFID_PC4,
        o_instruction => o_IFID_instruction

    );

    o_IFID_PC4 <= s_IFID_PC4;

    ID_EX : ID_EX_Reg PORT MAP(
        i_CLK          => i_CLK,
        i_RST          => i_RST,
        i_stall        => i_stall_IDEX,
        i_PC4          => s_IFID_PC4,
        i_readA        => x"00000000",
        i_readB        => x"00000000",
        i_signExtImmed => x"00000000",
        i_inst20_16    => b"00000",
        i_inst15_11    => b"00000",
        i_RegDst       => '0',
        i_RegWrite     => '0',
        i_memToReg     => '0',
        i_MemWrite     => '0',
        i_ALUSrc       => '0',
        i_ALUOp        => b"0000",
        i_jal          => '0',
        i_halt         => '0',

        o_PC4          => s_IDEX_PC4,
        o_readA        => OPEN,
        o_readB        => OPEN,
        o_signExtImmed => OPEN,
        o_inst20_16    => OPEN,
        o_inst15_11    => OPEN,
        o_RegDst       => OPEN,
        o_RegWrite     => OPEN,
        o_MemWrite     => OPEN,
        o_ALUSrc       => OPEN,
        o_ALUOp        => OPEN,
        o_jal          => OPEN,
        o_halt         => OPEN
    );

    o_IDEX_PC4 <= s_IDEX_PC4;

    EXMEM : EX_MEM PORT MAP(
        i_CLK => i_CLK,
        i_RST => i_RST, -- Reset input
        -- i_WE     -- Write enable input
        i_stall    => i_stall_EXMEM,
        i_ALU      => x"00000000", -- ALU value input
        o_ALU      => OPEN,        -- ALU value output
        i_B        => x"00000000", --Mem address value
        o_B        => OPEN,        --Mem address value
        i_WrAddr   => b"00000",    --write address to register file
        o_WrAddr   => OPEN,
        i_MemWr    => '0',  --write to memory
        o_MemWr    => OPEN, --write to memory signal
        i_MemtoReg => '0',  --memory vs alu signal
        o_MemtoReg => OPEN,
        i_Halt     => '0', --Halt signal
        o_Halt     => OPEN,
        i_RegWr    => '0', --write enable for reg file
        o_RegWr    => OPEN,
        i_PC4      => s_IDEX_PC4, --pc+4
        o_PC4      => s_EXMEM_PC4
    );

    o_EXMEM_PC4 <= s_EXMEM_PC4;

    MEMWB : MEM_WB PORT MAP(
        i_CLK => i_CLK, -- Clock input
        i_RST => i_RST, -- Reset input
        --i_WE  =>        -- Write enable input
        i_stall    => i_stall_MEMWB,
        i_ALU      => x"00000000", -- ALU value input
        o_ALU      => OPEN,        -- ALU value output
        i_Mem      => x"00000000", --Mem value
        o_Mem      => OPEN,        --Mem value
        i_WrAddr   => b"00000",    --write address to register file
        o_WrAddr   => OPEN,
        i_MemtoReg => '0', --memory vs alu signal
        o_MemtoReg => OPEN,
        i_Halt     => '0', --halt signal
        o_Halt     => OPEN,
        i_RegWr    => '0', --write enable for reg file
        o_RegWr    => OPEN,
        i_PC4      => s_EXMEM_PC4, --pc+4
        o_PC4      => s_MEMWB_PC4
    );

    o_MEMWB_PC4 <= s_MEMWB_PC4;

END structural;