-- IF_ID_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of IF/ID stage
-- of the pipelined processor
-------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
ENTITY ID_EX_reg IS
	PORT (
		i_CLK   : IN STD_LOGIC;
		i_RST   : IN STD_LOGIC;
		i_stall : IN STD_LOGIC; --stall control
		
		i_PC4          : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		i_readA        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		i_readB        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		i_signExtImmed : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		--i_JA           : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
		i_inst20_16 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		i_inst15_11 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		i_RegDst    : IN STD_LOGIC;
		i_RegWrite  : IN STD_LOGIC;
		i_memToReg  : IN STD_LOGIC;
		i_MemWrite  : IN STD_LOGIC;
		i_ALUSrc    : IN STD_LOGIC;
		i_ALUOp     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		i_jal       : IN STD_LOGIC;
		i_halt      : IN STD_LOGIC;
		i_RS		: in std_logic_vector(4 downto 0);
		i_memRd        : in std_logic;
		--i_control             : in std_logic_vector(14 downto 0);
		o_RS		: out std_logic_vector(4 downto 0);
		o_PC4          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		o_readA        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		o_readB        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		o_signExtImmed : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		--o_JA           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --jump address
		o_inst20_16 : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		o_inst15_11 : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		--o_control             : out std_logic_vector(14 downto 0));
		o_RegDst   : OUT STD_LOGIC;
		o_RegWrite : OUT STD_LOGIC;
		o_memToReg : OUT STD_LOGIC;
		o_MemWrite : OUT STD_LOGIC;
		o_ALUSrc   : OUT STD_LOGIC;
		o_ALUOp    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		o_jal      : OUT STD_LOGIC;
		o_halt     : OUT STD_LOGIC;
		o_memRd : out std_logic
	);
END ID_EX_reg;

ARCHITECTURE structural OF ID_EX_reg IS

	COMPONENT dffg_N IS
		GENERIC (N : INTEGER := 32);
		PORT (
			i_CLK : IN STD_LOGIC;                          -- Clock input
			i_RST : IN STD_LOGIC;                          -- Reset input
			i_WE  : IN STD_LOGIC;                          -- Write enable input
			i_D   : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);   -- Data value input
			o_Q   : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)); -- Data value output
	END COMPONENT;

	COMPONENT dffg IS
		PORT (
			i_CLK : IN STD_LOGIC;   -- Clock input
			i_RST : IN STD_LOGIC;   -- Reset input
			i_WE  : IN STD_LOGIC;   -- Write enable input
			i_D   : IN STD_LOGIC;   -- Data value input
			o_Q   : OUT STD_LOGIC); -- Data value output
	END COMPONENT;
	
	SIGNAL s_stall : STD_LOGIC;

BEGIN
	s_stall <= NOT i_stall;

	x1 : dffg_N
	GENERIC MAP(N => 32)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_PC4,
		o_Q   => o_PC4);

	x2 : dffg_N
	GENERIC MAP(N => 32)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_readA,
		o_Q   => o_readA);

	x3 : dffg_N
	GENERIC MAP(N => 32)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_readB,
		o_Q   => o_readB);

	x4 : dffg_N
	GENERIC MAP(N => 32)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_signExtImmed,
		o_Q   => o_signExtImmed);

	--	x4_5 : dffg_N
	--	GENERIC MAP(N => 32)
	--	PORT MAP(
	--		i_CLK => i_CLK,
	--		i_RST => i_RST,
	--		i_WE  => s_stall,
	--		i_D   => i_JA,
	--		o_Q   => o_JA);

	x5 : dffg_N
	GENERIC MAP(N => 5)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_inst20_16,
		o_Q   => o_inst20_16);

	x6 : dffg_N
	GENERIC MAP(N => 5)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_inst15_11,
		o_Q   => o_inst15_11);

	-- x7: dffg_N
	--	generic map(N => 15)
	--	port map(i_CLK 	=> i_CLK,
	--		 i_RST 	=> i_RST,
	--		 i_WE	=> s_stall,
	--		 i_D	=> i_control,
	--		 o_Q	=> o_control);

	x8 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_RegDst,
		o_Q   => o_RegDst
	);

	x9 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_RegWrite,
		o_Q   => o_RegWrite
	);

	x10 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_memToReg,
		o_Q   => o_memToReg
	);

	x11 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_MemWrite,
		o_Q   => o_MemWrite
	);

	x12 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_ALUSrc,
		o_Q   => o_ALUSrc
	);

	x13 : dffg_N
	GENERIC MAP(N => 4)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_ALUOp,
		o_Q   => o_ALUOp);

	x14 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_jal,
		o_Q   => o_jal
	);

	x15 : dffg
	PORT MAP(

		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_halt,
		o_Q   => o_halt
	);

	x16 : dffg
	port map (
            i_CLK => i_CLK,
            i_RST => i_RST,
            i_WE  => s_stall,
            i_D   => i_memRd,
            o_Q   => o_memRd
   );

   x17 : dffg_N
	GENERIC MAP(N => 5)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_RS,
		o_Q   => o_RS);

END structural;