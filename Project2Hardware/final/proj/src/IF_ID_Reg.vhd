-- IF_ID_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of IF/ID stage
-- of the pipelined processor
-------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
ENTITY IF_ID_reg IS
	PORT (
		i_CLK         : IN STD_LOGIC;
		i_RST         : IN STD_LOGIC;
		i_stall       : IN STD_LOGIC;
		i_PC4         : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		i_instruction : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		o_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		o_instruction : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));

END IF_ID_reg;

ARCHITECTURE structural OF IF_ID_reg IS

	COMPONENT dffg_N IS
		GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
		PORT (
			i_CLK : IN STD_LOGIC;                          -- Clock input
			i_RST : IN STD_LOGIC;                          -- Reset input
			i_WE  : IN STD_LOGIC;                          -- Write enable input
			i_D   : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);   -- Data value input
			o_Q   : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)); -- Data value output
	END COMPONENT;

signal s_stall : std_logic;

BEGIN
	s_stall <= not i_stall;


	x1 : dffg_N
	GENERIC MAP(N => 32)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_stall,
		i_D   => i_PC4,
		o_Q   => o_PC4);

	x2 : dffg_N
	GENERIC MAP(N => 32)
	PORT MAP(
		i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE => s_stall,
		i_D => i_instruction,
		o_Q => o_instruction);

END structural;