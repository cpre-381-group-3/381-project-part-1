-- hazard_detection.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the hazard detection unit
-- for the MIPS single cycle processor
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity hazard_detection is
    port(   i_jump_ID          : in std_logic; --Control Hazard
            i_branch_ID        : in std_logic; --Control Hazard
    
            i_rAddrA 	  	: in std_logic_vector(4 downto 0); --Data Hazard
            i_rAddrB	  	    : in std_logic_vector(4 downto 0); --Data Hazard
            i_wAddr_ID      : in std_logic_vector(4 downto 0); -- Write Address for ID
            i_wAddr_EX      : in std_logic_vector(4 downto 0); -- Write Address for MEM
            i_wE_ID    : in std_logic; -- Write enable for ID
            i_wE_EX    : in std_logic; -- Write enable for MEM
    
            o_stall    		: out std_logic;
            o_flush : out std_logic); -- 1 if stalling is true
    end hazard_detection;

    ARCHITECTURE mixed of hazard_detection is
    BEGIN
        process(i_jump_ID,   
            i_branch_ID,                    
            i_rAddrA, 	  
            i_rAddrB,	  	
            i_wAddr_ID,  
            i_wAddr_EX,  
            i_wE_ID,
            i_wE_EX)
        BEGIN
        --case1
        if((i_wE_ID = '1' AND i_rAddrA = i_wAddr_ID AND i_rAddrA /= "00000") OR (i_wE_EX = '1' AND i_rAddrA = i_wAddr_EX AND i_rAddrA /= "00000")) then --RegReadA Hazard
            o_stall <= '1';
            o_flush <= '0';
            
            
            --case2
        elsif ((i_wE_ID = '1' AND i_rAddrB = i_wAddr_ID AND i_rAddrB /= "00000") OR (i_wE_EX = '1' AND i_rAddrB = i_wAddr_EX AND i_rAddrB /= "00000")) then --RegReadB Hazard
            o_stall <= '1';
            o_flush <= '0';

            --case3
     elsif(
         i_jump_ID   = '1' OR
         i_branch_ID = '1') then
             o_flush <= '1'; --Control Hazard 
             o_stall <= '0';

             --case4
     else
         o_stall <= '0';
         o_flush <= '0';

     end if;
        end process;
    end mixed;