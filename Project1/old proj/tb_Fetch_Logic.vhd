LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Fetch_Logic IS
END tb_Fetch_Logic;

ARCHITECTURE tb OF tb_Fetch_Logic IS
    CONSTANT N : INTEGER := 32;
    COMPONENT Fetch_Logic
        PORT (
            i_PC          : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --Program counter
            i_branch_addr : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential branch address
            i_jump_addr   : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential jump address
            i_jr          : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential jump register address
            i_jr_select   : IN STD_LOGIC;                        --selector for jump register
            i_branch      : IN STD_LOGIC;                        --selects if a branch instruction is active
            i_zero        : IN STD_LOGIC;                        --if branch, determines if you should branch
            i_jump        : IN STD_LOGIC;                        --selector for j or jal
            o_PC          : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0) --program counter output
        );
    END COMPONENT;

    -- Temporary signals to connect to the component.
    SIGNAL s_jr_select, s_branch, s_zero, s_jump            : STD_LOGIC;
    SIGNAL s_i_PC, s_branch_addr, s_jump_addr, s_jr, s_o_PC : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);

BEGIN

    DUT : Fetch_Logic
    PORT MAP(
        i_PC => s_i_PC,
        i_branch_addr => s_branch_addr,
        i_jump_addr => s_jump_addr,
        i_jr => s_jr,
        i_jr_select => s_jr_select,
        i_branch => s_branch,
        i_zero => s_zero,
        i_jump => s_jump,
        o_PC              => s_o_PC);

    -- Testbench process  
    P_TB : PROCESS
    BEGIN

        --Base address values
        s_i_PC <= x"10000000";
        s_branch_addr <= x"00001000";
        s_jump_addr <= x"000FF000";
        s_jr <= x"20000000";

        --Test selecting PC+4, no special cases
        s_jr_select <= '0';
        s_branch <= '0';
        s_zero <= '0';
        s_jump <= '0';
        WAIT FOR 100 ns;

        --Branch is active, but condition isn't met: still PC+4
        s_jr_select <= '0';
        s_branch <= '0';
        s_zero <= '1';
        s_jump <= '0';
        WAIT FOR 100 ns;

        --Use PC+4 + Branch
        s_jr_select <= '0';
        s_branch <= '1';
        s_zero <= '1';
        s_jump <= '0';
        WAIT FOR 100 ns;

        --Use J
        s_jr_select <= '0';
        s_branch <= '0';
        s_zero <= '0';
        s_jump <= '1';
        WAIT FOR 100 ns;

        --Use JR
        s_jr_select <= '1';
        s_branch <= '0';
        s_zero <= '0';
        s_jump <= '1';
        WAIT FOR 100 ns;

    END PROCESS;
END tb;
