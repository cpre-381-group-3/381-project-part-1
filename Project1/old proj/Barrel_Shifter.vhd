LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Barrel_Shifter IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
        i_data             : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_logic_arithmetic : IN STD_LOGIC;                        -- 0 for logical, 1 for arithmetic (sign bit)
        i_left_right       : IN STD_LOGIC;                        --0 for shift left, 1 for shift right
        i_shamt            : IN STD_LOGIC_VECTOR(4 DOWNTO 0);     --shift amount
        o_Out              : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0) --output of the shifter
    );

END Barrel_Shifter;

ARCHITECTURE structural OF Barrel_Shifter IS

    COMPONENT mux2t1_N IS
        PORT (
            i_S  : IN STD_LOGIC;
            i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));
    END COMPONENT;

    SIGNAL s_RG0 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 0 right shift gate
    SIGNAL s_RG1 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 1 right shift gate
    SIGNAL s_RG2 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 2 right shift gate
    SIGNAL s_RG3 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 3 right shift gate
    SIGNAL s_RG4 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 4 right shift gate
    SIGNAL s_LG0 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 0 right left gate
    SIGNAL s_LG1 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 1 right left gate
    SIGNAL s_LG2 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 2 right left gate
    SIGNAL s_LG3 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 3 right left gate
    SIGNAL s_LG4 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --output of 4 right left gate

    SIGNAL s_sign : STD_LOGIC; --sign bit used for arithmetic signals

    --Right shifted signals
    SIGNAL s_shiftR0 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftR1 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftR2 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftR3 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftR4 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);

    --Left shifted signals
    SIGNAL s_shiftL0 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftL1 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftL2 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftL3 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    SIGNAL s_shiftL4 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
BEGIN

    s_sign <= '0' WHEN (i_logic_arithmetic = '0')ELSE
        i_data(31);

    s_shiftR0 <= s_sign & i_data(31 DOWNTO 1);

    --Each right shifter
    right_shifter_0 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(0),
        i_D0 => i_data,
        i_D1 => s_shiftR0,
        o_O  => s_RG0);

    s_shiftR1 <= s_sign & s_sign & s_RG0(31 DOWNTO 2);

    right_shifter_1 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(1),
        i_D0 => s_RG0,
        i_D1 => s_shiftR1,
        o_O  => s_RG1);

    s_shiftR2 <= s_sign & s_sign & s_sign & s_sign & s_RG1(31 DOWNTO 4);

    right_shifter_2 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(2),
        i_D0 => s_RG1,
        i_D1 => s_shiftR2,
        o_O  => s_RG2);

    s_shiftR3 <= s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_RG2(31 DOWNTO 8);

    right_shifter_3 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(3),
        i_D0 => s_RG2,
        i_D1 => s_shiftR3,
        o_O  => s_RG3);

    s_shiftR4 <= s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign & s_sign &
        s_RG3(31 DOWNTO 16);

    right_shifter_4 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(4),
        i_D0 => s_RG3,
        i_D1 => s_shiftR4,
        o_O  => s_RG4);

    s_shiftL0 <= i_data(30 DOWNTO 0) & '0';

    --Each left shifter
    left_shifter_0 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(0),
        i_D0 => i_data,
        i_D1 => s_shiftL0,
        o_O  => s_LG0);

    s_shiftL1 <= s_LG0(29 DOWNTO 0) & b"00";

    left_shifter_1 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(1),
        i_D0 => s_LG0,
        i_D1 => s_shiftL1,
        o_O  => s_LG1);

    s_shiftL2 <= s_LG1(27 DOWNTO 0) & b"0000";

    left_shifter_2 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(2),
        i_D0 => s_LG1,
        i_D1 => s_shiftL2,
        o_O  => s_LG2);

    s_shiftL3 <= s_LG2(23 DOWNTO 0) & b"00000000";

    left_shifter_3 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(3),
        i_D0 => s_LG2,
        i_D1 => s_shiftL3,
        o_O  => s_LG3);

    s_shiftL4 <= s_LG3(15 DOWNTO 0) & x"0000";

    left_shifter_4 : mux2t1_N
    PORT MAP(
        i_S  => i_shamt(4),
        i_D0 => s_LG3,
        i_D1 => s_shiftL4,
        o_O  => s_LG4);

    final_select : mux2t1_N
    PORT MAP(
        i_S  => i_left_right,
        i_D0 => s_LG4,
        i_D1 => s_RG4,
        o_O  => o_Out
    );

END structural;
