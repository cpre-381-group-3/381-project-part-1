LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_MySecondMIPSDatapath IS
    GENERIC (gCLK_HPER : TIME := 50 ns);
END tb_MySecondMIPSDatapath;

ARCHITECTURE tb OF tb_MySecondMIPSDatapath IS

    -- Calculate the clock period as twice the half-period
    CONSTANT cCLK_PER : TIME := gCLK_HPER * 2;

    CONSTANT DATA_WIDTH : INTEGER := 32;
    CONSTANT ADDR_WIDTH : INTEGER := 10;
    CONSTANT N          : INTEGER := 32;

    COMPONENT MySecondMIPSDatapath
        PORT (
            nAdd_Sub  : IN STD_LOGIC;                     --Function Decider
            ALUSrc    : IN STD_LOGIC;                     --immediate decider
            i_I       : IN STD_LOGIC_VECTOR(15 DOWNTO 0); --immediate value
            i_write   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_en      : IN STD_LOGIC;
            clk       : IN STD_LOGIC;
            reset     : IN STD_LOGIC;
            i_readA   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_readB   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_we      : IN STD_LOGIC; --control the write enable of memory storage
            zero_sign : IN STD_LOGIC; --determines if the immediate value is zero or sign extended
            add_load  : IN STD_LOGIC; -- determines if the arithmetic or memory value should be written to the register file
            o_S       : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_C       : OUT STD_LOGIC);
    END COMPONENT;

    -- Temporary signals to connect to the component.
    SIGNAL s_nAdd_Sub, s_ALUSrc, s_en, s_CLK, s_reset, s_we, s_zero_sign, s_add_load, s_C : STD_LOGIC;
    SIGNAL s_write, s_readA, s_readB                                                      : STD_LOGIC_VECTOR(4 DOWNTO 0);
    SIGNAL s_I                                                                            : STD_LOGIC_VECTOR(15 DOWNTO 0);
    SIGNAL s_S                                                                            : STD_LOGIC_VECTOR(31 DOWNTO 0);

BEGIN

    DUT : MySecondMIPSDatapath
    PORT MAP(
        nAdd_Sub  => s_nAdd_Sub,
        ALUSrc    => s_ALUSrc,
        i_I       => s_I,
        i_write   => s_write,
        i_en      => s_en,
        clk       => s_CLK,
        i_we      => s_we,
        zero_sign => s_zero_sign,
        add_load  => s_add_load,
        reset     => s_reset,
        i_readA   => s_readA,
        i_readB   => s_readB,
        o_S       => s_S,
        o_C       => s_C);

    -- This process sets the clock value (low for gCLK_HPER, then high
    -- for gCLK_HPER). Absent a "wait" command, processes restart 
    -- at the beginning once they have reached the final statement.
    P_CLK : PROCESS
    BEGIN
        s_CLK <= '0';
        WAIT FOR gCLK_HPER;
        s_CLK <= '1';
        WAIT FOR gCLK_HPER;
    END PROCESS;

    -- Testbench process  
    P_TB : PROCESS
    BEGIN
        -- Reset all values
        s_reset <= '1';
        s_en    <= '0';
        WAIT FOR cCLK_PER;
        s_reset <= '0';
        WAIT FOR cCLK_PER;

        --addi $25, $0, 0 #Load &A into $25
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00000";
        s_I         <= x"0000";
        s_en        <= '1';
        s_write     <= "11001";
        WAIT FOR cCLK_PER;

        --addi $26, $0, 256 # Load &B into $26
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00000";
        s_I         <= x"0100";
        s_en        <= '1';
        s_write     <= "11010";
        WAIT FOR cCLK_PER;

        --lw $1, 0($25) # Load A[0] into $1
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0000";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;

        --lw $2, 4($25) # Load A[1] into $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0001";
        s_en        <= '1';
        s_write     <= "00010";
        WAIT FOR cCLK_PER;

        --add $1, $1, $2 # $1 = $1 + $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '0';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00001";
        s_readB     <= "00010";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;
        s_en <= '0';

        --sw $1, 0($26) # Store $1 into B[0]
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '1';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11010";
        s_readB <= "00001";
        s_I         <= x"0000";
        s_en        <= '0';
        s_write     <= "00000";
        WAIT FOR cCLK_PER;

        --lw $2, 8($25) # Load A[2] into $2
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0002"; --offset of 2, or 8 bytes
        s_en        <= '1';
        s_write     <= "00010";
        WAIT FOR cCLK_PER;

        --add $1, $1, $2 # $1 = $1 + $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '0';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00001";
        s_readB     <= "00010";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;
        s_en <= '0';

        --sw $1, 4($26) # Store $1 into B[1]
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '1';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11010";
        s_readB <= "00001";
        s_I         <= x"0001";
        s_en        <= '0';
        s_write     <= "00000";
        WAIT FOR cCLK_PER;

        --lw $2, 12($25) # Load A[3] into $2
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0003"; --offset of 3, or 12 bytes
        s_en        <= '1';
        s_write     <= "00010";
        WAIT FOR cCLK_PER;

        ---add $1, $1, $2 # $1 = $1 + $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '0';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00001";
        s_readB     <= "00010";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;
        s_en <= '0';

        --sw $1, 8($26) # Store $1 into B[2]
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '1';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11010";
        s_readB <= "00001";
        s_I         <= x"0002";
        s_en        <= '0';
        s_write     <= "00000";
        WAIT FOR cCLK_PER;

        --lw $2, 16($25) # Load A[4] into $2
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0004"; 
        s_en        <= '1';
        s_write     <= "00010";
        WAIT FOR cCLK_PER;

        ---add $1, $1, $2 # $1 = $1 + $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '0';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00001";
        s_readB     <= "00010";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;
        s_en <= '0';

        --sw $1, 12($26) # Store $1 into B[3]
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '1';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11010";
        s_readB <= "00001";
        s_I         <= x"0003";
        s_en        <= '0';
        s_write     <= "00000";
        WAIT FOR cCLK_PER;

        --lw $2, 20($25) # Load A[5] into $2
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0005"; 
        s_en        <= '1';
        s_write     <= "00010";
        WAIT FOR cCLK_PER;

        ---add $1, $1, $2 # $1 = $1 + $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '0';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00001";
        s_readB     <= "00010";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;
        s_en <= '0';

        --sw $1, 16($26) # Store $1 into B[4]
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '1';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11010";
        s_readB <= "00001";
        s_I         <= x"0004";
        s_en        <= '0';
        s_write     <= "00000";
        WAIT FOR cCLK_PER;

        --lw $2, 24($25) # Load A[6] into $2
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '1';
        s_zero_sign <= '0';
        s_readA     <= "11001";
        s_I         <= x"0006"; 
        s_en        <= '1';
        s_write     <= "00010";
        WAIT FOR cCLK_PER;

        --add $1, $1, $2 # $1 = $1 + $2
        --set the function correctly
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '0';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00001";
        s_readB     <= "00010";
        s_en        <= '1';
        s_write     <= "00001";
        WAIT FOR cCLK_PER;
        s_en <= '0';

        --addi $27, $0, 512 # Load &B[256] into $27
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '0';
        s_add_load  <= '0';
        s_zero_sign <= '0';
        s_readA     <= "00000";
        s_I         <= x"0200";
        s_en        <= '1';
        s_write     <= "11011";
        WAIT FOR cCLK_PER;

        --sw $1, -4($27) # Store $1 into B[255]
        s_nAdd_Sub  <= '0';
        s_ALUSrc    <= '1';
        s_we        <= '1';
        s_add_load  <= '1';
        s_zero_sign <= '1';
        s_readA     <= "11011";
        s_readB <= "00001";
        s_I         <= x"FFFF";
        s_en        <= '0';
        s_write     <= "00000";
        WAIT FOR cCLK_PER;

        WAIT;
    END PROCESS;

END tb;
