--Adder subtractor file

library IEEE;
use IEEE.std_logic_1164.all;

entity MyFirstMIPSDatapath is
  -- generic(N : integer := 32); -- Using 16 because that is the max of the 2to1 MUX
  port(nAdd_Sub          : in std_logic; --Function Decider
        ALUSrc : in std_logic; --immediate decider
       i_I : in std_logic_vector(31 downto 0); --immediate value
       -- i_data : in std_logic_vector(31 downto 0);
    i_write : in std_logic_vector(4 downto 0);
    i_en : in std_logic;
    clk : in std_logic;
    reset : in std_logic;
    i_readA : in std_logic_vector(4 downto 0);
    i_readB : in std_logic_vector(4 downto 0);
    o_S          : out std_logic_vector(31 downto 0);
       o_C          : out std_logic);

end MyFirstMIPSDatapath;

architecture structural of MyFirstMIPSDatapath is

    component Adder_Subtractor_I is
      generic(N : integer := 32);
      port(nAdd_Sub          : in std_logic; --Function Decider
      ALUSrc : in std_logic; --immediate decider
     i_A         : in std_logic_vector(N-1 downto 0);
     i_B         : in std_logic_vector(N-1 downto 0);
     i_I : in std_logic_vector(N-1 downto 0); --immediate value
     o_S          : out std_logic_vector(N-1 downto 0);
     o_C          : out std_logic);
    end component;
  
    component MIPS_regfile is
      --generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
    port(i_data : in std_logic_vector(31 downto 0);
    i_write : in std_logic_vector(4 downto 0);
    i_en : in std_logic;
    clk : in std_logic;
    reset : in std_logic;
    i_readA : in std_logic_vector(4 downto 0);
    i_readB : in std_logic_vector(4 downto 0);
    o_A : out std_logic_vector(31 downto 0);
    o_B : out std_logic_vector(31 downto 0));
      end component;

      --signals for a and b from the register file

      signal s_A : std_logic_vector(31 downto 0);
      signal s_B : std_logic_vector(31 downto 0);

      --output signal to check the sum

      signal s_Out : std_logic_vector(31 downto 0);

      begin 


      RegFile: MIPS_regfile
        port MAP(i_data    => s_Out,
        i_write   => i_write,
        i_en => i_en,
        clk => clk,
        reset => reset,
        i_readA => i_readA,
        i_readB => i_readB,
        o_A => s_A,
        o_B => s_B);

      Adder: Adder_Subtractor_I 
        port MAP(
          nAdd_Sub => nAdd_Sub,
          ALUSrc => ALUSrc,
          i_A => s_A,
          i_B => s_B,
          i_I => i_I,
          o_S => s_Out,
          o_C => o_C
        );

        o_S <= s_Out;

        end structural;