LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY MySecondMIPSDatapath IS
    -- generic(N : integer := 32); -- Using 16 because that is the max of the 2to1 MUX
    PORT (
        nAdd_Sub  : IN STD_LOGIC;                     --Function Decider
        ALUSrc    : IN STD_LOGIC;                     --immediate decider
        i_I       : IN STD_LOGIC_VECTOR(15 DOWNTO 0); --immediate value
        i_write   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
        i_en      : IN STD_LOGIC;
        clk       : IN STD_LOGIC;
        reset     : IN STD_LOGIC;
        i_readA   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
        i_readB   : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
        i_we      : IN STD_LOGIC; --control the write enable of memory storage
        zero_sign : IN STD_LOGIC; --determines if the immediate value is zero or sign extended (immExt)
        add_load  : IN STD_LOGIC; -- determines if the arithmetic or memory value should be written to the register file (mem2reg)
        o_S       : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --Output sum (sum_ints)
        o_C       : OUT STD_LOGIC
    );

END MySecondMIPSDatapath;

ARCHITECTURE structural OF MySecondMIPSDatapath IS

    CONSTANT DATA_WIDTH : INTEGER := 32;
    CONSTANT ADDR_WIDTH : INTEGER := 10;
    CONSTANT N          : INTEGER := 32;

    COMPONENT Adder_Subtractor_I IS
        GENERIC (N : INTEGER := 32);
        PORT (
            nAdd_Sub : IN STD_LOGIC; --Function Decider
            ALUSrc   : IN STD_LOGIC; --immediate decider
            i_A      : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_B      : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_I      : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --immediate value
            o_S      : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_C      : OUT STD_LOGIC);
    END COMPONENT;

    COMPONENT MIPS_regfile IS
        --generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
        PORT (
            i_data  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            i_write : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_en    : IN STD_LOGIC;
            clk     : IN STD_LOGIC;
            reset   : IN STD_LOGIC;
            i_readA : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            i_readB : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            o_A     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            o_B     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    END COMPONENT;

    COMPONENT MIPS_extender IS
        PORT (
            i_sign : IN STD_LOGIC;                       --determines if the value must be sign or zero extended
            i_in   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);   --value to be extended
            o_O    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)); --value that has been extended
    END COMPONENT;

    COMPONENT mem IS
        GENERIC (
            DATA_WIDTH : NATURAL := 32;
            ADDR_WIDTH : NATURAL := 10
        );

        PORT (
            clk  : IN STD_LOGIC;
            addr : IN STD_LOGIC_VECTOR((ADDR_WIDTH - 1) DOWNTO 0);
            data : IN STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0);
            we   : IN STD_LOGIC := '1';
            q    : OUT STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0)
        );

    END COMPONENT;

    COMPONENT mux2t1_n IS
        GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
        PORT (
            i_S  : IN STD_LOGIC;
            i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));

    END COMPONENT;

    --signals

    SIGNAL s_A : STD_LOGIC_VECTOR(31 DOWNTO 0);
    SIGNAL s_B : STD_LOGIC_VECTOR(31 DOWNTO 0);

    --output signal for the sum

    SIGNAL s_Sum : STD_LOGIC_VECTOR(31 DOWNTO 0);

    SIGNAL s_q : STD_LOGIC_VECTOR(31 DOWNTO 0); --signal for memory output

    SIGNAL s_I : STD_LOGIC_VECTOR(31 DOWNTO 0); --signal for immediate after extention

    SIGNAL s_Out : STD_LOGIC_VECTOR(31 DOWNTO 0); --signal for data to be written to register file

BEGIN
    RegFile : MIPS_regfile
    PORT MAP(
        i_data  => s_Out,
        i_write => i_write,
        i_en    => i_en,
        clk     => clk,
        reset   => reset,
        i_readA => i_readA,
        i_readB => i_readB,
        o_A     => s_A,
        o_B     => s_B);

    Adder : Adder_Subtractor_I
    PORT MAP(
        nAdd_Sub => nAdd_Sub,
        ALUSrc   => ALUSrc,
        i_A      => s_A,
        i_B      => s_B,
        i_I      => s_I,
        o_S      => s_Sum,
        o_C      => o_C
    );

    Extender : MIPS_extender
    PORT MAP(
        i_sign => zero_sign,
        i_in   => i_I,
        o_O    => s_I
    );

    Memory : mem
    PORT MAP(
        clk  => clk,
        addr => s_Sum((ADDR_WIDTH - 1) downto 0),
        data => s_B,
        we   => i_we,
        q    => s_q
    );

    datamux : mux2t1_N
    PORT MAP(
        i_S  => add_load,
        i_D0 => s_Sum,
        i_D1 => s_q,
        o_O  => s_Out
    );

    o_S <= s_Out;

END structural;
