

.data
data1:  .word 5
data2:  .word 3
result: .word 0

.text
.globl main

main:
	# Data transfer instructions
	lw $t0, data1   # Load data1 into $t0
	lw $t1, data2   # Load data2 into $t1

	# Arithmetic instructions
	add $t2, $t0, $t1   # $t2 = $t0 + $t1 = 5 + 3 = 8
	addi $t3, $t0, 2	# $t3 = $t0 + 2 = 5 + 2 = 7
	addu $t4, $t0, $t1  # $t4 = $t0 + $t1 = 5 + 3 = 8
	sub $t5, $t0, $t1   # $t5 = $t0 - $t1 = 5 - 3 = 2

	# Logical instructions
	and $t6, $t0, $t1   # $t6 = $t0 & $t1 = 5 & 3 = 1
	andi $t7, $t0, 3	# $t7 = $t0 & 3 = 5 & 3 = 1
	or $t8, $t0, $t1	# $t8 = $t0 | $t1 = 5 | 3 = 7
	ori $t9, $t0, 1 	# $t9 = $t0 | 1 = 5 | 1 = 5

	# Set less than instructions
	slt $t0, $t0, $t1   # $t0 = ($t0 < $t1) ? 1 : 0 = (5 < 3) ? 1 : 0 = 0
	slti $t1, $t1, 6	# $t1 = ($t1 < 6) ? 1 : 0 = (3 < 6) ? 1 : 0 = 1

	# Shift instructions
	sll $t2, $t2, 2 	# $t2 = $t2 << 2 = 8 << 2 = 32
	srl $t3, $t3, 2 	# $t3 = $t3 >> 2 (logical) = 7 >> 2 = 1
	sra $t4, $t4, 2 	# $t4 = $t4 >> 2 (arithmetic) = 8 >> 2 = 2

	# Branch instructions
	bne $t5, $t6, not_equal #$t5 /= $t6

	not_equal:
	jal jump_and_link_target
	j exit

	jump_and_link_target:
	sw $t1, result   # Store the value of $t1 into result
	
	beq $t5, $t6, equal
	
	equal:
	j jump_target
	
	
	jump_target:
	jr $ra


	# Exit
	exit:
	li $v0, 10    	# Exit syscall
	syscall



