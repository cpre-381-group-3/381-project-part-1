library IEEE;
use IEEE.std_logic_1164.all;

entity Logical_Gates is
  generic(N : integer := 32); -- Using 16 because that is the max of the 2to1 MUX
  port(
       i_A         : in std_logic_vector(N-1 downto 0);
       i_B         : in std_logic_vector(N-1 downto 0);
       o_OR, o_AND, o_XOR, o_NOR          : out std_logic_vector(N-1 downto 0));

end Logical_Gates;

Architecture looped of Logical_Gates is
Begin
    PROCESS --or, and, xor, nor
    BEGIN
      FOR i IN 0 TO 31 LOOP
        o_OR(i)  <= i_A(i) OR i_B(i);
        o_AND(i) <= i_A(i) AND i_B(i);
        o_XOR(i) <= i_A(i) XOR i_B(i);
        o_NOR(i) <= i_A(i) NOR i_B(i);
      END LOOP;
      WAIT;
    END PROCESS;
    end looped;