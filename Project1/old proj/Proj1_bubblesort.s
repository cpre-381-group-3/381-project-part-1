.data
    
    data:   .word   4, 2, 10, 8, 6, 1, 3, 9, 7, 5
    size:   .word   10

.text
main:
    add $t0, $zero, $zero #i = $t0 = 0
 
    lw $t2, size # $t2 = size = 10
    subu $t2, $t2, 1 #size = size-1
    
    #load the base address of the data array into $s0
    la $s0, data
    
   
    loop_i:
    addi $t3, $t2, 0 #copy size to $t3
    slt $at, $t0, $t2 #sets $at to 1 if i<size-1    
    bne $at, $zero, loop_i_continue #moves into i loop if i<size-1
    j exit_loop_i #i>size-1
   
    loop_i_continue:
    sub $t3, $t3, $t0 #size = size-1-i
    add $t1, $zero, $zero #j = $t1 = 0
   
    loop_j:
     slt $at, $t1, $t3 #sets $at to 1 if j<size-1-i
     bne $at, $zero, loop_j_continue #moves into j loop if j<size-1-i
     j exit_loop_j #j>=size-1-i
     
    loop_j_continue:
   
    #swap function
   sll $t4, $t1, 2 #j offset in $t4
   add $t5, $t4, 4 #j+1 offset in $t5
   add $t6, $t4, $s0 #address of j data
   add $t7, $t5, $s0 #addresss of j+1 data
   
   lw $t8, 0($t6) #j data
   lw $t9, 0($t7) #j+1 data
   
   slt $at, $t9, $t8 #is j+1 < j
   bne $at, $zero, swap
   
        #addi $a0, $t1, 0
	# Print the value in register $t1
	#li $v0, 1
	#syscall
    addi $t1, $t1, 1 #j=j+1
    j loop_j
    
   swap: 
   sw $t9, 0($t6) #j+1 data into memory at address j
   sw $t8, 0($t7) #j data into memory at address j+1
   addi $t1, $t1, 1 #j=j+1
   j loop_j
   
   
    
   
    
    

   
   
    exit_loop_j:
    add $t1, $zero, $zero #j back to 0
    addi $t0, $t0, 1 #i+1
    j loop_i
   
   
    exit_loop_i:
    li $v0, 10 #end program
    syscall


