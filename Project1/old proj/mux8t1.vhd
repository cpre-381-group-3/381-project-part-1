LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY mux8t1 IS
  PORT (
    i_S  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    i_D0 : IN STD_LOGIC;
    i_D1 : IN STD_LOGIC;
    i_D2 : IN STD_LOGIC;
    i_D3 : IN STD_LOGIC;
    i_D4 : IN STD_LOGIC;
    i_D5 : IN STD_LOGIC;
    i_D6 : IN STD_LOGIC;
    i_D7 : IN STD_LOGIC;
    o_O  : OUT STD_LOGIC);

END mux8t1;

architecture dataflow of mux8t1 is
  begin
  o_O <= i_D0 when (i_S = "000") else
  i_D1 when (i_S = "001") else
  i_D2 when (i_S = "010") else
  i_D3 when (i_S = "011") else
  i_D4 when (i_S = "100") else
  i_D5 when (i_S = "101") else
  i_D6 when (i_S = "110") else
  i_D7 when (i_S = "111") else
  '0';
  end dataflow;
