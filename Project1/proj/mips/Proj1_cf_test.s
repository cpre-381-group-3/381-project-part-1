.data
stack:  .space 20 # Reserve space for a stack of depth 5 (5 * 4 bytes)
equal_str: .asciiz "equal\n" # Define the string "equal"
notequal_str: .asciiz "Not Equal\n" # Define the string "notequal"
jump_str: .asciiz "WEEEEE\n" # Define the string "jump"


.text

main:
    # Initialize stack pointer (sp)
    la $sp, stack

    # Push values onto the stack
    li $t0, 10
    sw $t0, 0($sp)    # Push 10 onto the stack
    addi $sp, $sp, -4 # Move the stack pointer down

    li $t1, 20
    sw $t1, 0($sp)    # Push 20 onto the stack
    addi $sp, $sp, -4 # Move the stack pointer down

    li $t2, 30
    sw $t2, 0($sp)    # Push 30 onto the stack
    addi $sp, $sp, -4 # Move the stack pointer down

    li $t6, 30
    sw $t6, 0($sp)    # Push 30 onto the stack
    addi $sp, $sp, -4 # Move the stack pointer down

    # Pop values from the stack
    lw $t3, 0($sp)    # Pop the top value (30)
    addi $sp, $sp, 4  # Move the stack pointer up

    lw $t4, 0($sp)    # Pop the top value (30)
    addi $sp, $sp, 4  # Move the stack pointer up

    lw $t5, 0($sp)    # Pop the top value (20)
    addi $sp, $sp, 4  # Move the stack pointer up

    lw $t7, 0($sp)    # Pop the top value (10)
    addi $sp, $sp, 4  # Move the stack pointer up

     #Jal and jr
    jal biiig_jump

    # Check if values are equal
    beq $t3, $t4, equal   # Branch to 'equal' if t3 equals t4 (they do)
    bne $t4, $t5, not_equal  # Branch to 'not_equal' if t4 is not equal to t5 (they do not)

   
equal:
    # Print the word "equal"
    la $a0, equal_str  # Load the address of the "equal" string
    li $v0, 4          # System call code for printing a string
    syscall
    

not_equal:
    # Print the word "Not Equal"
    la $a0, notequal_str  # Load the address of the "notequal" string
    li $v0, 4          # System call code for printing a string
    syscall
    j exit

biiig_jump: 
	#Print the word "WEEEEE"
	la $a0, jump_str  # Load the address of the "notequal" string
	li $v0, 4          # System call code for printing a string
	syscall
	jr $ra
	
exit:
    # Exit the program
    li $v0, 10
    syscall
halt
