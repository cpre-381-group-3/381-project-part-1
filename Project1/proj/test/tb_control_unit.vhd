-------------------------------------------------------------------------
-- tb_control_unit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for the MIPS control unit
-- 
-------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

entity tb_control_unit is
end tb_control_unit;

architecture behavior of tb_control_unit is

  component Control_Unit
	PORT (
		i_opCode    : IN STD_LOGIC_VECTOR(5 DOWNTO 0); --MIPS instruction opcode (6 bits wide)
		i_funct   : IN STD_LOGIC_VECTOR(5 DOWNTO 0); --MIPS instruction function code (6 bits wide) used for R-Type instructions
		o_RegDst    : OUT STD_LOGIC;
		o_RegWrite  : OUT STD_LOGIC;
		o_memToReg  : OUT STD_LOGIC;
		o_memWrite  : OUT STD_LOGIC;
		o_ALUSrc    : OUT STD_LOGIC;
		o_ALUOp     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		o_signed    : OUT STD_LOGIC;
		o_addSub    : OUT STD_LOGIC;
		o_shiftType : OUT STD_LOGIC;
		o_shiftDir  : OUT STD_LOGIC;
		o_bne       : OUT STD_LOGIC;
		o_beq       : OUT STD_LOGIC;
		o_j         : OUT STD_LOGIC;
		o_jr        : OUT STD_LOGIC;
		o_jal       : OUT STD_LOGIC;
		o_branch    : OUT STD_LOGIC;
		o_jump      : OUT STD_LOGIC;
		o_lui       : OUT STD_LOGIC;
		o_halt      : OUT STD_LOGIC);
  end component;

-- Temporary signals to connect to the component.
    SIGNAL s_RegDst, s_RegWrite, s_memToReg, s_memWrite, s_ALUSrc, s_signed, s_addSub, s_shiftType, s_shiftDir, s_bne, s_beq, s_j, s_jr, s_jal, s_branch, s_jump, s_lui, s_halt : STD_LOGIC;
    SIGNAL s_opCode, s_funct : STD_LOGIC_VECTOR(5 DOWNTO 0);
    SIGNAL s_ALUOp : STD_LOGIC_VECTOR(3 DOWNTO 0);

begin

  DUT: Control_Unit 
  port map(
		i_opCode    => s_opCode,
		i_funct   => s_funct,
		o_RegDst    => s_RegDst,
		o_RegWrite  => s_RegWrite,
		o_memToReg  => s_memToReg,
		o_memWrite  => s_memWrite,
		o_ALUSrc    => s_ALUSrc,
		o_ALUOp     => s_ALUOp,
		o_signed    => s_signed,
		o_addSub    => s_addSub,
		o_shiftType => s_shiftType,
		o_shiftDir  => s_shiftDir,
		o_bne       => s_bne,
		o_beq       => s_beq,
		o_j         => s_j,
		o_jr        => s_jr,
		o_jal       => s_jal,
		o_branch    => s_branch,
		o_jump      => s_jump,
		o_lui       => s_lui,
		o_halt      => s_halt  );


  
  -- Test run 2700
  P_TB: process
  begin
    -- test add
    s_opcode <= "000000";
    s_funct  <= "010000";
    wait for 100 ns;

    -- test addu
    s_opcode <= "000000";
    s_funct  <= "100001";
    wait for 100 ns;

    -- test and
    s_opcode <= "000000";
    s_funct  <= "100100";
    wait for 100 ns;

    -- test nor
    s_opcode <= "000000";
    s_funct  <= "100111";
    wait for 100 ns;

    -- test xor
    s_opcode <= "000000";
    s_funct  <= "100111";
    wait for 100 ns;

    -- test or
    s_opcode <= "000000";
    s_funct  <= "100101";
    wait for 100 ns;

    -- test slt
    s_opcode <= "000000";
    s_funct  <= "101010";
    wait for 100 ns;

    -- test sll
    s_opcode <= "000000";
    s_funct  <= "000000";
    wait for 100 ns;

    -- test srl
    s_opcode <= "000000";
    s_funct  <= "000010";
    wait for 100 ns;

    -- test sra
    s_opcode <= "000000";
    s_funct  <= "000011";
    wait for 100 ns;

    -- test sub
    s_opcode <= "000000";
    s_funct  <= "100010";
    wait for 100 ns;

    -- test subu
    s_opcode <= "000000";
    s_funct  <= "100011";
    wait for 100 ns;

    -- test addi
    s_opcode <= "001000";
    s_funct  <= "000000";
    wait for 100 ns;

    -- test addiu
    s_opcode <= "001001";
    wait for 100 ns;

    -- test andi
    s_opcode <= "001100";
    wait for 100 ns;

    -- test xori
    s_opcode <= "001110";
    wait for 100 ns;

    -- test ori
    s_opcode <= "001101";
    wait for 100 ns;

    -- test slti
    s_opcode <= "001010";
    wait for 100 ns;

    -- test lui
    s_opcode <= "001111";
    wait for 100 ns;

    -- test beq
    s_opcode <= "000100";
    wait for 100 ns;

    -- test bne
    s_opcode <= "000101";
    wait for 100 ns;

    -- test lw
    s_opcode <= "100011";
    wait for 100 ns;

    -- test sw
    s_opcode <= "101011";
    wait for 100 ns;

    -- test j
    s_opcode <= "000010";
    wait for 100 ns;

    -- test jal
    s_opcode <= "000011";
    wait for 100 ns;

    -- test jr
    s_opcode <= "000000";
    s_funct  <= "001000";
    wait for 100 ns;

    -- test halt
    s_opcode <= "010100";
    wait for 100 ns;

    wait;
  end process;
  
end behavior;
