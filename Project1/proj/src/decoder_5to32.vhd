library IEEE;
use IEEE.std_logic_1164.all;

entity decoder_5to32 is
  port(
    i_input : in std_logic_vector(4 downto 0);
    i_en : in std_logic;
    o_O : out std_logic_vector(31 downto 0));

end decoder_5to32;

architecture behavioral of decoder_5to32 is
    signal mixed : std_logic_vector(5 downto 0);
    begin  
    mixed <= i_en & i_input;
        o_O <= (0 => '1', others => '0') when (mixed = "100000") else
        (1 => '1', others => '0') when (mixed = "100001") else
        (2 => '1', others => '0') when (mixed = "100010") else
        (3 => '1', others => '0') when (mixed = "100011") else
        (4 => '1', others => '0') when (mixed = "100100") else
        (5 => '1', others => '0') when (mixed = "100101") else
        (6 => '1', others => '0') when (mixed = "100110") else
        (7 => '1', others => '0') when (mixed = "100111") else
        (8 => '1', others => '0') when (mixed = "101000") else
        (9 => '1', others => '0') when (mixed = "101001") else
        (10 => '1', others => '0') when (mixed = "101010") else
        (11 => '1', others => '0') when (mixed = "101011") else
        (12 => '1', others => '0') when (mixed = "101100") else
        (13 => '1', others => '0') when (mixed = "101101") else
        (14 => '1', others => '0') when (mixed = "101110") else
        (15 => '1', others => '0') when (mixed = "101111") else
        (16 => '1', others => '0') when (mixed = "110000") else
        (17 => '1', others => '0') when (mixed = "110001") else
        (18 => '1', others => '0') when (mixed = "110010") else
        (19 => '1', others => '0') when (mixed = "110011") else
        (20 => '1', others => '0') when (mixed = "110100") else
        (21 => '1', others => '0') when (mixed = "110101") else
        (22 => '1', others => '0') when (mixed = "110110") else
        (23 => '1', others => '0') when (mixed = "110111") else
        (24 => '1', others => '0') when (mixed = "111000") else
        (25 => '1', others => '0') when (mixed = "111001") else
        (26 => '1', others => '0') when (mixed = "111010") else
        (27 => '1', others => '0') when (mixed = "111011") else
        (28 => '1', others => '0') when (mixed = "111100") else
        (29 => '1', others => '0') when (mixed = "111101") else
        (30 => '1', others => '0') when (mixed = "111110") else
        (31 => '1', others => '0') when (mixed = "111111") else
        "00000000000000000000000000000000";

        end behavioral;
