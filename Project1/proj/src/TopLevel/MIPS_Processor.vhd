-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a MIPS_Processor  
-- implementation.

-- 01/29/2019 by H3::Design created.
-------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

LIBRARY work;
USE work.MIPS_types.ALL;

ENTITY MIPS_Processor IS
  GENERIC (N : INTEGER := DATA_WIDTH);
  PORT (
    iCLK      : IN STD_LOGIC;
    iRST      : IN STD_LOGIC;
    iInstLd   : IN STD_LOGIC;
    iInstAddr : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    iInstExt  : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    oALUOut   : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

END MIPS_Processor;
ARCHITECTURE structure OF MIPS_Processor IS

  -- Required data memory signals
  SIGNAL s_DMemWr   : STD_LOGIC;                        -- TODO: use this signal as the final active high data memory write enable signal
  SIGNAL s_DMemAddr : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the final data memory address input
  SIGNAL s_DMemData : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the final data memory data input
  SIGNAL s_DMemOut  : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the data memory output

  -- Required register file signals 
  SIGNAL s_RegWr     : STD_LOGIC;                        -- TODO: use this signal as the final active high write enable input to the register file
  SIGNAL s_RegWrAddr : STD_LOGIC_VECTOR(4 DOWNTO 0);     -- TODO: use this signal as the final destination register address input
  SIGNAL s_RegWrData : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the final data memory data input

  -- Required instruction memory signals
  SIGNAL s_IMemAddr     : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  SIGNAL s_NextInstAddr : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as your intended final instruction memory address input.
  SIGNAL s_Inst         : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); -- TODO: use this signal as the instruction signal 

  -- Required halt signal -- for simulation
  SIGNAL s_Halt : STD_LOGIC; -- TODO: this signal indicates to the simulation that intended program execution has completed. (Opcode: 01 0100)

  -- Required overflow signal -- for overflow exception detection
  SIGNAL s_Ovfl : STD_LOGIC; -- TODO: this signal indicates an overflow exception would have been initiated

  -- TODO: You may add any additional signals or components your implementation 
  --       requires below this comment

  COMPONENT mem IS
    GENERIC (
      ADDR_WIDTH : INTEGER;
      DATA_WIDTH : INTEGER);
    PORT (
      clk  : IN STD_LOGIC;
      addr : IN STD_LOGIC_VECTOR((ADDR_WIDTH - 1) DOWNTO 0);
      data : IN STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0);
      we   : IN STD_LOGIC := '1';
      q    : OUT STD_LOGIC_VECTOR((DATA_WIDTH - 1) DOWNTO 0));
  END COMPONENT;

  COMPONENT MIPS_regfile IS
    PORT (
      i_data  : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_write : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_en    : IN STD_LOGIC;
      clk     : IN STD_LOGIC;
      reset   : IN STD_LOGIC;
      i_readA : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_readB : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_A     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_B     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT ALU IS
    PORT (
      i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_ALUOP    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      i_shamt    : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_resultF  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_CarryOut : OUT STD_LOGIC;
      o_Overflow : OUT STD_LOGIC;
      o_zero     : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT Fetch_Logic IS
    PORT (
      i_PC          : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --Program counter
      i_branch_addr : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --potential branch address
      i_jump_addr   : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --potential jump address
      i_jr          : IN STD_LOGIC_VECTOR(31 DOWNTO 0); --potential jump register address
      i_jr_select   : IN STD_LOGIC;                     --selector for jump register
      i_branch      : IN STD_LOGIC;                     --selects if a branch instruction is active
      i_zero        : IN STD_LOGIC;                     --if branch, determines if you should branch
      i_jump        : IN STD_LOGIC;                     --selector for j or jal
      o_PC          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_PC4         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT MIPS_extender IS
    PORT (
      i_sign : IN STD_LOGIC;                     --determines if the value must be sign or zero extended
      i_in   : IN STD_LOGIC_VECTOR(15 DOWNTO 0); --value to be extended
      o_O    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT mux2t1_N IS
  Generic (N : integer);
    PORT (
      i_S  : IN STD_LOGIC;
      i_D0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT mux2t1_5bit IS
  Generic (N : integer);
    PORT (
      i_S  : IN STD_LOGIC;
      i_D0 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(4 DOWNTO 0));
  END COMPONENT;

  --PC register
  COMPONENT dffg_N IS
    PORT (
      i_CLK : IN STD_LOGIC;                     -- Clock input
      i_RST : IN STD_LOGIC;                     -- Reset input
      i_WE  : IN STD_LOGIC;                     -- Write enable input
      i_D   : IN STD_LOGIC_VECTOR(31 DOWNTO 0); -- Data value input
      o_Q   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  COMPONENT Control_Unit IS
    PORT (
      i_opCode    : IN STD_LOGIC_VECTOR(5 DOWNTO 0); --MIPS instruction opcode (6 bits wide)
      i_funct   : IN STD_LOGIC_VECTOR(5 DOWNTO 0); --MIPS instruction function code (6 bits wide) used for R-Type instructions
      o_RegDst    : OUT STD_LOGIC;
      o_RegWrite  : OUT STD_LOGIC;
      o_memToReg  : OUT STD_LOGIC;
      o_memWrite  : OUT STD_LOGIC;
      o_ALUSrc    : OUT STD_LOGIC;
      o_ALUOp     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      o_signed    : OUT STD_LOGIC;
      o_addSub    : OUT STD_LOGIC;
      o_shiftType : OUT STD_LOGIC;
      o_shiftDir  : OUT STD_LOGIC;
      o_bne       : OUT STD_LOGIC;
      o_beq       : OUT STD_LOGIC;
      o_j         : OUT STD_LOGIC;
      o_jr        : OUT STD_LOGIC;
      o_jal       : OUT STD_LOGIC;
      o_branch    : OUT STD_LOGIC;
      o_jump      : OUT STD_LOGIC;
      o_lui       : OUT STD_LOGIC;
      o_halt      : OUT STD_LOGIC);
  END COMPONENT;

  --signals 
  signal s_RegDst, s_memToReg, s_memWrite, s_ALUSrc, s_j, s_jr, s_jal, s_branch, s_jump : std_logic;
  signal s_ALUOp : std_logic_vector(3 downto 0);
  signal s_signed, s_lui, s_addSub, s_shiftType, s_shiftDir, s_bne, s_beq : std_logic;
  signal s_RegA : std_logic_vector(31 downto 0); 
  signal s_rtrd : std_logic_vector(4 downto 0);
  signal s_immediate : std_logic_vector(31 downto 0);
  signal s_PC, s_PCR : std_logic_vector(31 downto 0);
  signal s_PC4 : std_logic_vector(31 downto 0);
  signal s_zero, s_CarryOut : std_logic;
  signal s_ALUB, s_aluORmem : std_logic_vector(31 downto 0);
  
  

BEGIN

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  WITH iInstLd SELECT
    s_IMemAddr <= s_NextInstAddr WHEN '0',
    iInstAddr WHEN OTHERS;
  IMem : mem
  GENERIC MAP(
    ADDR_WIDTH => ADDR_WIDTH,
    DATA_WIDTH => N)
  PORT MAP(
    clk  => iCLK,
    addr => s_IMemAddr(11 DOWNTO 2),
    data => iInstExt,
    we   => iInstLd,
    q    => s_Inst);

  DMem : mem
  GENERIC MAP(
    ADDR_WIDTH => ADDR_WIDTH,
    DATA_WIDTH => N)
  PORT MAP(
    clk  => iCLK,
    addr => s_DMemAddr(11 DOWNTO 2),
    data => s_DMemData,
    we   => s_DMemWr,
    q    => s_DMemOut);

  -- TODO: Ensure that s_Halt is connected to an output control signal produced from decoding the Halt instruction (Opcode: 01 0100)
  -- TODO: Ensure that s_Ovfl is connected to the overflow output of your ALU

  -- TODO: Implement the rest of your processor below this comment! 

  
 
  RegFile : MIPS_regfile
  port map(
        i_data  => s_RegWrData,
        i_write => s_RegWrAddr,
        i_en    => s_RegWr,
        clk     => iCLK,
        reset   => iRST,
        i_readA => s_Inst(25 downto 21),
        i_readB => s_Inst(20 downto 16),
        o_A     => s_RegA,
        o_B     => s_DMemData
  );

  rtrdMUX : mux2t1_5bit
  generic map (N => 5)
  port map(
    i_S  => s_RegDst,
      i_D0 => s_Inst(20 downto 16),
      i_D1 => s_Inst(15 downto 11),
      o_O  => s_rtrd
  );

  writeMUX : mux2t1_5bit
  generic map(n => 5)
  port map(
    i_S => s_jal,
    i_D0 => s_rtrd,
    i_D1 => "11111",
    o_O => s_RegWrAddr
  );

  Control : Control_Unit
  port map(
    i_opCode    => s_Inst(31 downto 26),
      i_funct   => s_Inst(5 downto 0),
      o_RegDst    => s_RegDst,
      o_RegWrite  => s_RegWr,
      o_memToReg  => s_memToReg,
      o_memWrite  => s_DMemWr,
      o_ALUSrc    => s_ALUSrc,
      o_ALUOp     => s_ALUOp,
      o_signed    => s_signed,
      o_addSub    => s_addSub,
      o_shiftType => s_shiftType,
      o_shiftDir  => s_shiftDir,
      o_bne       => s_bne,
      o_beq       => s_beq,
      o_j         => s_j,
      o_jr        => s_jr,
      o_jal       => s_jal,
      o_branch    => s_branch,
      o_jump      => s_jump,
      o_lui       => s_lui,
      o_halt      => s_Halt
  );

  PC : dffg_N
  port map(
    i_CLK => iCLK,
      i_RST => '0',
      i_WE  => '1',
      i_D   => s_PCR,
      o_Q   => s_NextInstAddr
  );

  PCRESET : mux2t1_N
  generic map(N => 32)
  port map(
    i_S => iRST,
    i_D0 => s_PC,
    i_D1 => x"00400000",
    o_O => s_PCR  
  );


  Fetch : Fetch_Logic
  port map(
    i_PC          => s_NextInstAddr,
      i_branch_addr => s_immediate,
      i_jump_addr   => s_Inst,
      i_jr          => s_RegA,
      i_jr_select   => s_jr,
      i_branch      => s_branch,
      i_zero        => s_zero,
      i_jump        => s_jump,
      o_PC          => s_PC,
      o_PC4         => s_PC4
  );

  SignExtend : MIPS_extender 
  port map(
    i_sign => s_signed,
      i_in   => s_Inst(15 downto 0),
      o_O    => s_immediate
  );

  immediateMUX : mux2t1_N
  generic map(N => 32)
  port map(
    i_S => s_ALUSrc,
    i_D0 => s_DMemData,
    i_D1 => s_immediate,
    o_O => s_ALUB
  );

  mainALU : ALU
  port map(
      i_A        => s_RegA,
      i_B        => s_ALUB,
      i_ALUOP    => s_ALUOp,
      i_shamt    => s_Inst(10 downto 6),
      o_resultF  => s_DMemAddr,
      o_CarryOut => s_CarryOut,
      o_Overflow => s_Ovfl,
      o_zero     => s_zero
  );

oALUOut <= s_DMemAddr;

  memtoreg : mux2t1_N
  generic map (N => 32)
  port map(
    i_S => s_memToReg,
    i_D0 => s_DMemAddr,
    i_D1 => s_DMemOut,
    o_O => s_aluORmem
  );

  raMUX : mux2t1_N
  generic map(N => 32)
  port map(
    i_S => s_jal,
    i_D0 => s_aluORmem,
    i_D1 => s_PC4,
    o_O => s_RegWrData
  );


END structure;
