

--This is built off of the framework provided, as specified in the lab instructions
--Added to by Cael Schreier
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Full_Adder is
  port(i_A          : in std_logic;
       i_B         : in std_logic;
       i_C         : in std_logic;
       o_S          : out std_logic;
       o_C          : out std_logic);

end Full_Adder;

architecture structural of Full_Adder is


  component andg2 is
    port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
  end component;

  component org2 is
    port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
  end component; 
  
  component xorg2 is    
    port(i_A         : in std_logic;
    i_B             : in std_logic;
    o_F             : out std_logic);
    end component;

  --signals for stored values
  signal x1     : std_logic; --xor a / b
  signal an1      : std_logic; --first and
  signal a2     : std_logic; --second and

begin

  ---------------------------------------------------------------------------
  -- xor a/b 
  ---------------------------------------------------------------------------
 
  xor1: xorg2
    port MAP(i_A    => i_A,
    i_B             => i_B,
    o_F               => x1);
  ---------------------------------------------------------------------------
  -- And1
  ---------------------------------------------------------------------------

  and1: andg2
  port MAP(i_A    => i_A,
  i_B               => i_B,
  o_F               => an1);

  ---------------------------------------------------------------------------
  -- And2
  ---------------------------------------------------------------------------

  and2: andg2
  port MAP(i_A    => x1,
  i_B               => i_C,
  o_F               => a2);

  ---------------------------------------------------------------------------
  -- Final OR
  ---------------------------------------------------------------------------

  or1: org2
  port MAP(i_A    => an1,
  i_B               => a2,
  o_F               => o_C);

  ---------------------------------------------------------------------------
  -- xor for sum
  ---------------------------------------------------------------------------
 
  xor2: xorg2
    port MAP(i_A    => x1,
    i_B             => i_C,
    o_F               => o_S);


  
end structural;
