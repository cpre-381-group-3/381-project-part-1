library IEEE;
use IEEE.std_logic_1164.all;

entity MIPS_extender is
  port(i_sign          : in std_logic; --determines if the value must be sign or zero extended
       i_in         : in std_logic_vector(15 downto 0); --value to be extended
       o_O          : out std_logic_vector(31 downto 0)); --value that has been extended

end MIPS_extender;

architecture behavioral of MIPS_extender is

    signal s_O : std_logic_vector(31 downto 0);

    begin

    s_O <= x"0000" & i_in when (i_sign = '0')else --if 0 extended
    x"0000" & i_in when (i_in(15) = '0')else --if sign is 0
    x"FFFF" & i_in when (i_in(15) = '1')else --if sign is 1
    x"00000000"; --failsafe case, shouldn't occur

    o_O <= s_O;

end behavioral;