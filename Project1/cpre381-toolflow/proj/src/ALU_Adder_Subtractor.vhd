--Adder subtractor file

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY ALU_Adder_Subtractor IS
  GENERIC (N : INTEGER := 32); -- Using 16 because that is the max of the 2to1 MUX
  PORT (
    nAdd_Sub  : IN STD_LOGIC; --Function Decider
    i_A       : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    i_B       : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    o_S       : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    o_C, o_OF : OUT STD_LOGIC);

END ALU_Adder_Subtractor;

ARCHITECTURE structural OF ALU_Adder_Subtractor IS

  COMPONENT Full_Adder_N IS
    GENERIC (N : INTEGER := 32);
    PORT (
      i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_C        : IN STD_LOGIC;
      o_S        : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_C        : OUT STD_LOGIC;
      o_Overflow : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT Ones_Comp IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
      i_A : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_F : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));
  END COMPONENT;

  COMPONENT mux2t1_N IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
      i_S  : IN STD_LOGIC;
      i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));
  END COMPONENT;
  --constant N : integer := 32;
  SIGNAL n_B   : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --not B, used for subtraction
  SIGNAL mux_B : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --B after the multiplexor
BEGIN
  ---------------------------------------------------------------------------
  -- Inverter
  ---------------------------------------------------------------------------

  OnesComp1 : Ones_Comp
  PORT MAP(
    i_A => i_B,
    o_F => n_B);
  ---------------------------------------------------------------------------
  -- MUX
  ---------------------------------------------------------------------------

  MUX1 : mux2t1_N
  PORT MAP(
    i_S  => nAdd_Sub,
    i_D0 => i_B,
    i_D1 => n_B, --chooses regular b for a select of 0, and not b for a select of 1
    o_O  => mux_B);

  ---------------------------------------------------------------------------
  -- Full Adder
  ---------------------------------------------------------------------------

  FullAdderN1 : Full_Adder_N
  PORT MAP(
    i_A        => i_A,
    i_B        => mux_B,
    i_C        => nAdd_Sub,
    o_S        => o_S,
    o_C        => o_C,
    o_Overflow => o_OF);
END structural;
