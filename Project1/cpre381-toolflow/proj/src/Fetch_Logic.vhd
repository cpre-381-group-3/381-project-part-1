LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Fetch_Logic IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
        i_PC          : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --Program counter
        i_branch_addr : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential branch address
        i_jump_addr   : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential jump address
        i_jr          : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --potential jump register address
        i_jr_select   : IN STD_LOGIC;                        --selector for jump register
        i_branch      : IN STD_LOGIC;                        --selects if a branch instruction is active
        i_zero        : IN STD_LOGIC;                        --if branch, determines if you should branch
        i_jump        : IN STD_LOGIC;                        --selector for j or jal
        o_PC          : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --program counter output
        o_PC4 : OUT Std_logic_vector(N - 1 downto 0) --PC+4 output, used for jal instruction
        );

END Fetch_Logic;

ARCHITECTURE structural OF Fetch_Logic IS

    COMPONENT mux2t1_N IS
        PORT (
            i_S  : IN STD_LOGIC;
            i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));
    END COMPONENT;

    COMPONENT Barrel_Shifter IS
        PORT (
            i_data             : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_logic_arithmetic : IN STD_LOGIC;                          -- 0 for logical, 1 for arithmetic (sign bit)
            i_left_right       : IN STD_LOGIC;                          --0 for shift left, 1 for shift right
            i_shamt            : IN STD_LOGIC_VECTOR(4 DOWNTO 0);       --shift amount
            o_Out              : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)); --output of the shifter
    END COMPONENT;

    COMPONENT Full_Adder_N IS
        PORT (
            i_C        : IN STD_LOGIC; --initial carry
            i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_S        : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            o_C        : OUT STD_LOGIC;
            o_Overflow : OUT STD_LOGIC
        );
    END COMPONENT;

    SIGNAL s_PC4                 : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC+4
    SIGNAL s_branch_addr_shifted : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for the branch address after shifted left 2
    SIGNAL s_PC_branch           : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC+4+branchaddr
    SIGNAL s_j_addr_shifted      : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for jump address after shift
    SIGNAL s_PC_j_addr           : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC(31 - 28) + jump address(27 - 0)
    SIGNAL s_PC_or_Branch        : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC+4 or branch address
    SIGNAL s_PC_or_j             : STD_LOGIC_VECTOR(N - 1 DOWNTO 0); --signal for PC or branch or jump
    SIGNAL s_toBranch            : STD_LOGIC;                        --signal to determine if you should branch

BEGIN

    --PC+4
    PC_Adder : Full_Adder_N
    PORT MAP(
        i_C => '0', --no carry
        i_A => i_PC,
        i_B => x"00000004",
        o_S => s_PC4
    );
    o_PC4 <= s_PC4;

    --Shift the branch address by 2
    Branch_Shifter : Barrel_Shifter
    PORT MAP(
        i_data             => i_branch_addr,
        i_logic_arithmetic => '0',     --always logical shift
        i_left_right       => '0',     --left shift
        i_shamt            => "00010", --shift left 2
        o_Out              => s_branch_addr_shifted
    );

    --Add PC+4 + branch address
    Branch_PC_Adder : Full_Adder_N
    PORT MAP(
        i_C => '0', --no carry
        i_A => s_PC4,
        i_B => s_branch_addr_shifted,
        o_S => s_PC_branch
    );

    Jump_Address_Shift : Barrel_Shifter
    PORT MAP(
        i_data             => i_jump_addr,
        i_logic_arithmetic => '0',     --always logical shift
        i_left_right       => '0',     --left shift
        i_shamt            => "00010", --shift left 2
        o_Out              => s_j_addr_shifted
    );

    --Jump Address
    s_PC_j_addr <= s_PC4(31 DOWNTO 28) & s_j_addr_shifted(27 DOWNTO 0);

    s_toBranch <= i_branch AND i_zero;

    Branch_Select : mux2t1_N
    PORT MAP(
        i_S  => s_toBranch,
        i_D0 => s_PC4,
        i_D1 => s_PC_branch,
        o_O  => s_PC_or_Branch
    );

    Jump_Select : mux2t1_N
    PORT MAP(
        i_S  => i_jump,
        i_D0 => s_PC_or_Branch,
        i_D1 => s_PC_j_addr,
        o_O  => s_PC_or_j
    );

    JR_Select : mux2t1_N
    PORT MAP(
        i_S  => i_jr_select,
        i_D0 => s_PC_or_j,
        i_D1 => i_jr,
        o_O  => o_PC
    );

END structural;
