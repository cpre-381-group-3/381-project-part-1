LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Barrel_Shifter IS
END tb_Barrel_Shifter;

ARCHITECTURE tb OF tb_Barrel_Shifter IS
    CONSTANT N : INTEGER := 32;
    COMPONENT Barrel_Shifter
        PORT (
            i_data             : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
            i_logic_arithmetic : IN STD_LOGIC;                    -- 0 for logical, 1 for arithmetic (sign bit)
            i_left_right       : IN STD_LOGIC;                    --0 for shift left, 1 for shift right
            i_shamt            : IN STD_LOGIC_VECTOR(4 DOWNTO 0); --shift amount
            o_Out              : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)
        );
    END COMPONENT;

    -- Temporary signals to connect to the component.
    SIGNAL s_logic_arithmetic, s_left_right : STD_LOGIC;
    SIGNAL s_shamt                          : STD_LOGIC_VECTOR(4 DOWNTO 0);
    SIGNAL s_data, s_Out                    : STD_LOGIC_VECTOR(31 DOWNTO 0);

BEGIN

    DUT : Barrel_Shifter
    PORT MAP(
        i_data             => s_data,
        i_logic_arithmetic => s_logic_arithmetic,
        i_left_right       => s_left_right,
        i_shamt            => s_shamt,
        o_Out              => s_Out);

    -- Testbench process  
    P_TB : PROCESS
    BEGIN

        -- shift a value five bits to the left
        s_data             <= x"FFFF0000";
        s_logic_arithmetic <= '0';
        s_left_right       <= '0';
        s_shamt            <= b"00101";
        WAIT FOR 100 ns;

        --shift 16 bits to the left, should result in all 0s
        s_shamt <= b"10000";
        WAIT FOR 100 ns;

        --shift logical to the right by 7 bits
        s_left_right <= '1';
        s_shamt      <= b"00111";
        WAIT FOR 100 ns;

        --now shift arithmetic to the right by 3
        s_left_right       <= '1';
        s_logic_arithmetic <= '1';
        s_shamt            <= b"00011";
        WAIT FOR 100 ns;

        --shift right arithmetic to the right by 16
        s_left_right       <= '1';
        s_logic_arithmetic <= '1';
        s_shamt            <= b"10000";
        wait for 100 ns;

    END PROCESS;
END tb;