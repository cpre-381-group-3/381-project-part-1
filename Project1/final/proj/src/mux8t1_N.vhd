LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY mux8t1_N IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
        i_S  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D2 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D3 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D4 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D5 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D6 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        i_D7 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
        o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0));

END mux8t1_N;

ARCHITECTURE structural OF mux8t1_N IS

    COMPONENT mux8t1 IS
        PORT (
            i_S  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            i_D0 : IN STD_LOGIC;
            i_D1 : IN STD_LOGIC;
            i_D2 : IN STD_LOGIC;
            i_D3 : IN STD_LOGIC;
            i_D4 : IN STD_LOGIC;
            i_D5 : IN STD_LOGIC;
            i_D6 : IN STD_LOGIC;
            i_D7 : IN STD_LOGIC;
            o_O  : OUT STD_LOGIC);
    END COMPONENT;

BEGIN

    -- Instantiate N mux instances.
    G_NBit_MUX : FOR i IN 0 TO N - 1 GENERATE
        MUXI : mux8t1 PORT MAP(
            i_S  => i_S,
            i_D0 => i_D0(i),
            i_D1 => i_D1(i),
            i_D2 => i_D2(i),
            i_D3 => i_D3(i),
            i_D4 => i_D4(i),
            i_D5 => i_D5(i),
            i_D6 => i_D6(i),
            i_D7 => i_D7(i),
            o_O  => o_O(i)); -- ith instance's data output hooked up to ith data output.
    END GENERATE G_NBit_MUX;

END structural;
