--ALU File

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY ALU IS
  GENERIC (N : INTEGER := 32);
  PORT (
    i_A        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    i_B        : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    i_ALUOP    : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    i_shamt    : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    o_resultF  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    o_CarryOut : OUT STD_LOGIC;
    o_Overflow : OUT STD_LOGIC;
    o_zero     : OUT STD_LOGIC
  );

END ALU;

ARCHITECTURE mixed OF ALU IS

  COMPONENT ALU_Adder_Subtractor IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
      nAdd_Sub : IN STD_LOGIC;
      i_A      : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_B      : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_S      : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_C      : OUT STD_LOGIC;
      o_OF     : OUT STD_LOGIC);
  END COMPONENT;
  COMPONENT Barrel_Shifter IS
    GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
    PORT (
      i_data             : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_logic_arithmetic : IN STD_LOGIC;                        -- 0 for logical, 1 for arithmetic (sign bit)
      i_left_right       : IN STD_LOGIC;                        --0 for shift left, 1 for shift right
      i_shamt            : IN STD_LOGIC_VECTOR(4 DOWNTO 0);     --shift amount
      o_Out              : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0) --output of the shifter
    );
  END COMPONENT;

  --Detects if a value is 0, used to detect branch
  COMPONENT Zero_Detect IS
    PORT (
      i_F    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_zero : OUT STD_LOGIC);
  END COMPONENT;

  COMPONENT mux2t1_N IS
    GENERIC (N : INTEGER := 32);
    PORT (
      i_S  : IN STD_LOGIC;
      i_D0 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0)
    );
  END COMPONENT;

  --8 to 1 mux for 32 bit values
  COMPONENT mux8t1_N IS
    PORT (
      i_S  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      i_D0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_D7 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_O  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  --computes or, and, xor, and nor
  COMPONENT Logical_Gates IS
    PORT (
      i_A                       : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_B                       : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      o_OR, o_AND, o_XOR, o_NOR : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
  END COMPONENT;

  SIGNAL s_Adder_Out, s_Barrel_Output, s_OR, s_AND, s_XOR, s_NOR, s_set : STD_LOGIC_VECTOR(31 DOWNTO 0); --signals to be output to the mux
  SIGNAL s_zero, s_NOTzero, s_addSub_overflow, s_lui, s_nAdd_Sub, s_left_right,
  s_logic_arithmetic, s_bne, s_unsigned : STD_LOGIC; --internal control signals
  SIGNAL s_shamt                        : STD_LOGIC_VECTOR(4 DOWNTO 0);
  SIGNAL s_select                       : STD_LOGIC_VECTOR(2 DOWNTO 0); --final mux select
BEGIN

  --Logic to set signals based on ALUOP
  --NEED IMMEDIATE OPS SOON

  --Select add_sub function
  WITH i_ALUOP SELECT s_nAdd_Sub <=
    '1' WHEN "1110", --sub for beq
    '1' WHEN "1101", --sub for bne
    '1' WHEN "0011", --subu
    '1' WHEN "1111", --sub
    '1' WHEN "1000", --slt, use sub
    '0' WHEN OTHERS; --add for other instructions

  --lui select
  WITH i_ALUOP SELECT s_lui <=
    '1' WHEN "1001", --lui instruction
    '0' WHEN OTHERS;

  --left or right shift select
  WITH i_ALUOP SELECT s_left_right <=
    '1' WHEN "1011", --srl
    '1' WHEN "1100", --sra
    '0' WHEN OTHERS;

  --logical or arithmetic shift select
  WITH i_ALUOP SELECT s_logic_arithmetic <=
    '1' WHEN "1100", --sra
    '0' WHEN OTHERS;

  --check if branch not equal instruction
  WITH i_ALUOP SELECT s_bne <=
    '1' WHEN "1101", --bne instruction
    '0' WHEN OTHERS;

  --check if unsigned instruction
  WITH i_ALUOP SELECT s_unsigned <=
    '0' WHEN "0001", --addu
    '0' WHEN "0011", --subu
    '1' WHEN OTHERS; --consider overflow if not unsigned

  WITH i_ALUOP SELECT s_select <=
    "000" WHEN "1110", --beq, output doesn't matter
    "000" WHEN "1101", --bne, output doesn't matter
    "000" WHEN "0001", --addu, use adder output
    "000" WHEN "0011", --subu, use adder output
    "000" WHEN "0010", --add, use adder output
    "000" WHEN "1111", --sub, use adder output
    "011" WHEN "0100", --and, use and output
    "010" WHEN "0101", --or, use or output
    "100" WHEN "0110", --xor, use xor output
    "101" WHEN "0111", --nor, use nor output
    "001" WHEN "1001", --lui, use shifter output
    "110" WHEN "1000", --slt, use slt output
    "001" WHEN "1010", --sll, use shifter output
    "001" WHEN "1011", --srl, use shifter output
    "001" WHEN "1100", --sra, use shifter output
    "000" WHEN OTHERS; --doesn't matter, just use adder output

  --selects shamt or 16(used for lui)
  luiMUX : mux2t1_N
  GENERIC MAP(N => 5)
  PORT MAP(
    i_S  => s_lui,
    i_D0 => i_shamt,
    i_D1 => "10000",
    o_O  => s_shamt
  );

  shifter : Barrel_Shifter
  PORT MAP(
    i_data             => i_B,
    i_shamt            => s_shamt,
    i_left_right       => s_left_right,
    i_logic_arithmetic => s_logic_arithmetic,
    o_Out              => s_Barrel_Output);

  addsub : ALU_Adder_Subtractor
  GENERIC MAP(N => 32)
  PORT MAP(
    nAdd_Sub => s_nAdd_Sub,  --in std_logic;
    i_A      => i_A,         --in std_logic_vector(N-1 downto 0);
    i_B      => i_B,         --in std_logic_vector(N-1 downto 0);
    o_S      => s_Adder_Out, --out std_logic_vector(N-1 downto 0);
    o_C      => o_CarryOut,
    o_OF     => s_addSub_overflow);--out std_logic);

  o_Overflow <= s_addSub_overflow AND s_unsigned;

  zero_detector : Zero_Detect
  PORT MAP(
    i_F    => s_Adder_Out,
    o_zero => s_zero);

  --if zero is detected, set bit to high and xor with bne to determine if you should branch
  s_NOTzero <= NOT s_zero;
  o_zero    <= s_NOTzero XOR s_bne;

  gates : Logical_Gates
  PORT MAP(
    i_A   => i_A,
    i_B   => i_B,
    o_OR  => s_OR,
    o_AND => s_AND,
    o_XOR => s_XOR,
    o_NOR => s_NOR);

  --if 31st bit is negative (1) then set less than, 0 otherwise
  slt_value : mux2t1_N
  PORT MAP(
    i_S  => s_Adder_Out(31),
    i_D0 => x"00000000",
    i_D1 => x"00000001",
    o_O  => s_set
  );

  --select final output from the 7 options
  final_mux : mux8t1_N
  PORT MAP(
    i_S  => s_select,
    i_D0 => s_Adder_Out,
    i_D1 => s_Barrel_Output,
    i_D2 => s_OR,
    i_D3 => s_AND,
    i_D4 => s_XOR,
    i_D5 => s_NOR,
    i_D6 => s_set,
    i_D7 => x"00000000", --unused value, never selected for this project
    o_O  => o_resultF);

END mixed;
