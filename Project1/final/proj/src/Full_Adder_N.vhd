--Full adder schematic

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Full_Adder_N IS
  GENERIC (N : INTEGER := 32); -- Generic of type integer for input/output data width. Default value is 32.
  PORT (
    i_C : IN STD_LOGIC; --initial carry
    i_A : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    i_B : IN STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    o_S : OUT STD_LOGIC_VECTOR(N - 1 DOWNTO 0);
    o_C : OUT STD_LOGIC;
    o_Overflow : OUT std_logic);

END Full_Adder_N;

ARCHITECTURE structural OF Full_Adder_N IS

  COMPONENT Full_Adder IS
    PORT (
      i_A : IN STD_LOGIC;
      i_B : IN STD_LOGIC;
      i_C : IN STD_LOGIC;
      o_S : OUT STD_LOGIC;
      o_C : OUT STD_LOGIC);
  END COMPONENT;
  --constant N : integer := 32;
  SIGNAL carry : STD_LOGIC_VECTOR(N DOWNTO 0);
BEGIN

  --Initial carry
  carry(0) <= i_C;
  -- Instantiate N mux instances.
  G_NBit_FullAdder : FOR i IN 0 TO N - 1 GENERATE
    FullAdderI : Full_Adder PORT MAP(
      i_C => carry(i), -- The previous out carry is the new in carry
      i_A => i_A(i),   -- ith instance's data 0 input hooked up to ith data 0 input.
      i_B => i_B(i),   -- ith instance's data 1 input hooked up to ith data 1 input.
      o_S => o_S(i),
      o_C => carry(i + 1)); -- ith instance's data output hooked up to ith data output.
  END GENERATE G_NBit_FullAdder;
  o_C <= carry(N);
  o_Overflow <= carry(N) xor carry(N - 1);

END structural;
