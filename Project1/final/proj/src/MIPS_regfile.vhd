library IEEE;
use IEEE.std_logic_1164.all;

entity MIPS_regfile is
  port(
    i_data : in std_logic_vector(31 downto 0);
    i_write : in std_logic_vector(4 downto 0);
    i_en : in std_logic;
    clk : in std_logic;
    reset : in std_logic;
    i_readA : in std_logic_vector(4 downto 0);
    i_readB : in std_logic_vector(4 downto 0);
    o_A : out std_logic_vector(31 downto 0);
    o_B : out std_logic_vector(31 downto 0));

end MIPS_regfile;

architecture structural of MIPS_regfile is

    component decoder_5to32 is
        port(
            i_input : in std_logic_vector(4 downto 0);
            i_en : in std_logic;
            o_O : out std_logic_vector(31 downto 0));
    end component;

    component mux32to1_32b is
      port(
        i_S          : in std_logic_vector(4 downto 0);
       i_D0         : in std_logic_vector(31 downto 0);
       i_D1         : in std_logic_vector(31 downto 0);
       i_D2         : in std_logic_vector(31 downto 0);
       i_D3         : in std_logic_vector(31 downto 0);
       i_D4         : in std_logic_vector(31 downto 0);
       i_D5         : in std_logic_vector(31 downto 0);
       i_D6         : in std_logic_vector(31 downto 0);
       i_D7         : in std_logic_vector(31 downto 0);
       i_D8         : in std_logic_vector(31 downto 0);
       i_D9         : in std_logic_vector(31 downto 0);
       i_D10        : in std_logic_vector(31 downto 0);
       i_D11        : in std_logic_vector(31 downto 0);
       i_D12        : in std_logic_vector(31 downto 0);
       i_D13        : in std_logic_vector(31 downto 0);
       i_D14        : in std_logic_vector(31 downto 0);
       i_D15        : in std_logic_vector(31 downto 0);
       i_D16        : in std_logic_vector(31 downto 0);
       i_D17        : in std_logic_vector(31 downto 0);
       i_D18        : in std_logic_vector(31 downto 0);
       i_D19        : in std_logic_vector(31 downto 0);
       i_D20        : in std_logic_vector(31 downto 0);
       i_D21        : in std_logic_vector(31 downto 0);
       i_D22        : in std_logic_vector(31 downto 0);
       i_D23        : in std_logic_vector(31 downto 0);
       i_D24        : in std_logic_vector(31 downto 0);
       i_D25        : in std_logic_vector(31 downto 0);
       i_D26        : in std_logic_vector(31 downto 0);
       i_D27        : in std_logic_vector(31 downto 0);
       i_D28        : in std_logic_vector(31 downto 0);
       i_D29        : in std_logic_vector(31 downto 0);
       i_D30        : in std_logic_vector(31 downto 0);
       i_D31        : in std_logic_vector(31 downto 0);
       o_O          : out std_logic_vector(31 downto 0)
      );

      end component;

      component dffg_N is
        port(
        i_CLK        : in std_logic;     -- Clock input
        i_RST        : in std_logic;     -- Reset input
        i_WE         : in std_logic;     -- Write enable input
        i_D          : in std_logic_vector(31 downto 0);     -- Data value input
        o_Q          : out std_logic_vector(31 downto 0)
        );

        end component;

        --signal after decoder
      signal d_O : std_logic_vector(31 downto 0);

        --signals between mux

      signal s_D0 : std_logic_vector(31 downto 0);
      signal s_D1 : std_logic_vector(31 downto 0);
      signal s_D2 : std_logic_vector(31 downto 0);
      signal s_D3 : std_logic_vector(31 downto 0);
      signal s_D4 : std_logic_vector(31 downto 0);
      signal s_D5 : std_logic_vector(31 downto 0);
      signal s_D6 : std_logic_vector(31 downto 0);
      signal s_D7 : std_logic_vector(31 downto 0);
      signal s_D8 : std_logic_vector(31 downto 0);
      signal s_D9 : std_logic_vector(31 downto 0);
      signal s_D10 : std_logic_vector(31 downto 0);
      signal s_D11 : std_logic_vector(31 downto 0);
      signal s_D12 : std_logic_vector(31 downto 0);
      signal s_D13 : std_logic_vector(31 downto 0);
      signal s_D14 : std_logic_vector(31 downto 0);
      signal s_D15 : std_logic_vector(31 downto 0);
      signal s_D16 : std_logic_vector(31 downto 0);
      signal s_D17 : std_logic_vector(31 downto 0);
      signal s_D18 : std_logic_vector(31 downto 0);
      signal s_D19 : std_logic_vector(31 downto 0);
      signal s_D20 : std_logic_vector(31 downto 0);
      signal s_D21 : std_logic_vector(31 downto 0);
      signal s_D22 : std_logic_vector(31 downto 0);
      signal s_D23 : std_logic_vector(31 downto 0);
      signal s_D24 : std_logic_vector(31 downto 0);
      signal s_D25 : std_logic_vector(31 downto 0);
      signal s_D26 : std_logic_vector(31 downto 0);
      signal s_D27 : std_logic_vector(31 downto 0);
      signal s_D28 : std_logic_vector(31 downto 0);
      signal s_D29 : std_logic_vector(31 downto 0);
      signal s_D30 : std_logic_vector(31 downto 0);
      signal s_D31 : std_logic_vector(31 downto 0);


  
  begin

    --instantiate a 5to32 decoder

    decoder: decoder_5to32
      port map(
        i_input => i_write,
        i_en => i_en,
        o_O => d_O);    

    --instantiate 32 registers (dffg_N)
    --it would appear that I may have done a bit too much hard coding, but I believe that this works and I don't wanna redo everything else
    --There is definitely a more efficient way to do this though...

    dffg_0: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => '1',  --1 because it is always 0
              i_WE     => d_O(0),  
              i_D      => i_data,
              o_Q => s_D0); 
    
    dffg_1: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(1),  
              i_D      => i_data,
              o_Q => s_D1);

    dffg_2: dffg_N port map(
                  i_CLK      => clk,      -- Clock input
                  i_RST     => reset,  
                  i_WE     => d_O(2),  
                  i_D      => i_data,
                  o_Q => s_D2); 

    dffg_3: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(3),  
              i_D      => i_data,
              o_Q => s_D3); 
            
    dffg_4: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(4),  
              i_D      => i_data,
              o_Q => s_D4); 

    dffg_5: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(5),  
              i_D      => i_data,
              o_Q => s_D5); 

    dffg_6: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(6),  
              i_D      => i_data,
              o_Q => s_D6); 

    dffg_7: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(7),  
              i_D      => i_data,
              o_Q => s_D7); 

    dffg_8: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(8),  
              i_D      => i_data,
              o_Q => s_D8); 

    dffg_9: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(9),  
              i_D      => i_data,
              o_Q => s_D9); 

    dffg_10: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(10),  
              i_D      => i_data,
              o_Q => s_D10); 

    dffg_11: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(11),  
              i_D      => i_data,
              o_Q => s_D11); 

    dffg_12: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(12),  
              i_D      => i_data,
              o_Q => s_D12); 

    dffg_13: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(13),  
              i_D      => i_data,
              o_Q => s_D13); 

    dffg_14: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(14),  
              i_D      => i_data,
              o_Q => s_D14); 

    dffg_15: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(15),  
              i_D      => i_data,
              o_Q => s_D15); 

    dffg_16: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(16),  
              i_D      => i_data,
              o_Q => s_D16); 

    dffg_17: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(17),  
              i_D      => i_data,
              o_Q => s_D17); 

    dffg_18: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(18),  
              i_D      => i_data,
              o_Q => s_D18); 

    dffg_19: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(19),  
              i_D      => i_data,
              o_Q => s_D19); 

    dffg_20: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(20),  
              i_D      => i_data,
              o_Q => s_D20); 

    dffg_21: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(21),  
              i_D      => i_data,
              o_Q => s_D21); 

    dffg_22: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(22),  
              i_D      => i_data,
              o_Q => s_D22); 

    dffg_23: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(23),  
              i_D      => i_data,
              o_Q => s_D23);
              
    dffg_24: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(24),  
              i_D      => i_data,
              o_Q => s_D24);
              
    dffg_25: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(25),  
              i_D      => i_data,
              o_Q => s_D25); 

    dffg_26: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(26),  
              i_D      => i_data,
              o_Q => s_D26); 

    dffg_27: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(27),  
              i_D      => i_data,
              o_Q => s_D27); 

    dffg_28: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(28),  
              i_D      => i_data,
              o_Q => s_D28); 

    dffg_29: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(29),  
              i_D      => i_data,
              o_Q => s_D29); 

    dffg_30: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(30),  
              i_D      => i_data,
              o_Q => s_D30); 

    dffg_31: dffg_N port map(
              i_CLK      => clk,      -- Clock input
              i_RST     => reset,  
              i_WE     => d_O(31),  
              i_D      => i_data,
              o_Q => s_D31); 

    -- Instantiate MUXs

    MUXA: mux32to1_32b port map(
      i_S          => i_readA,
      i_D0         => s_D0,
      i_D1         => s_D1,
      i_D2         => s_D2,
      i_D3         => s_D3,
      i_D4         => s_D4,
      i_D5         => s_D5,
      i_D6         => s_D6,
      i_D7         => s_D7,
      i_D8         => s_D8,
      i_D9         => s_D9,
      i_D10        => s_D10,
      i_D11        => s_D11,
      i_D12        => s_D12,
      i_D13        => s_D13,
      i_D14        => s_D14,
      i_D15        => s_D15,
      i_D16        => s_D16,
      i_D17        => s_D17,
      i_D18        => s_D18,
      i_D19        => s_D19,
      i_D20        => s_D20,
      i_D21        => s_D21,
      i_D22        => s_D22,
      i_D23        => s_D23,
      i_D24        => s_D24,
      i_D25        => s_D25,
      i_D26        => s_D26,
      i_D27        => s_D27,
      i_D28        => s_D28,
      i_D29        => s_D29,
      i_D30        => s_D30,
      i_D31        => s_D31,
      o_O          => o_A);

    MUXB: mux32to1_32b port map(
      i_S          => i_readB,
      i_D0         => s_D0,
      i_D1         => s_D1,
      i_D2         => s_D2,
      i_D3         => s_D3,
      i_D4         => s_D4,
      i_D5         => s_D5,
      i_D6         => s_D6,
      i_D7         => s_D7,
      i_D8         => s_D8,
      i_D9         => s_D9,
      i_D10        => s_D10,
      i_D11        => s_D11,
      i_D12        => s_D12,
      i_D13        => s_D13,
      i_D14        => s_D14,
      i_D15        => s_D15,
      i_D16        => s_D16,
      i_D17        => s_D17,
      i_D18        => s_D18,
      i_D19        => s_D19,
      i_D20        => s_D20,
      i_D21        => s_D21,
      i_D22        => s_D22,
      i_D23        => s_D23,
      i_D24        => s_D24,
      i_D25        => s_D25,
      i_D26        => s_D26,
      i_D27        => s_D27,
      i_D28        => s_D28,
      i_D29        => s_D29,
      i_D30        => s_D30,
      i_D31        => s_D31,
      o_O          => o_B);

      end structural;
